// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatRealmWrapper",
    platforms: [
        .macOS("10.15"),
        .iOS("13.0")
    ],
    products: [
        .library(
            name: "MeerkatRealmWrapper",
            targets: ["MeerkatRealmWrapper"])
    ],
    dependencies: [
        .package(url: "https://github.com/realm/realm-cocoa", from: "4.3.2"),
        .package(url: "https://gitlab.com/MeerkatSync/meerkatschemedescriptor", from: "1.0.0"),
        .package(url: "https://gitlab.com/MeerkatSync/meerkatclientcore", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "MeerkatRealmWrapper",
            dependencies: ["RealmSwift", "MeerkatSchemeDescriptor", "MeerkatClientCore"]),
        .testTarget(
            name: "MeerkatRealmWrapperTests",
            dependencies: ["MeerkatRealmWrapper"]),
    ]
)
