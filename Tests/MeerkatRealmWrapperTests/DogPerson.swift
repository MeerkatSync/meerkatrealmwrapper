//
//  DogPerson.swift
//  
//
//  Created by Filip Klembara on 18/02/2020.
//

import RealmSwift
import MeerkatRealmWrapper
import Foundation
import MeerkatCore

final class DogPerson: Object, RealmSyncObject {
    @objc dynamic var group: SyncGroup? = nil
    
    @objc dynamic var isDeleted: Bool = false

    @objc dynamic var id: ObjectID = UUID().uuidString

    @objc dynamic var name: String = ""
    
    let dogs = List<Dog>()
}
