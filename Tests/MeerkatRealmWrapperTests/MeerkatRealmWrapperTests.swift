import XCTest
@testable import MeerkatRealmWrapper
@testable import MeerkatClientCore
import RealmSwift
import Combine

final class MeerkatRealmWrapperTests: XCTestCase {
    override func setUp() {
        Realm.Configuration.meerkatRealmConfiguration = .init(deleteRealmIfMigrationNeeded: true)
        let realm = try! tryGetRealm()
        try! realm.write {
            realm.deleteAll()
        }
    }

    func testExample() {
        let realm = try! tryGetRealm()
        let e = self.expectation(description: "Successful completion")
        let eNope = expectation(description: "Realm failure")
        eNope.isInverted = true
        let notif = MeerkatRealmNotificator(objects: [Person.self])

        var log: SyncLog? = nil
        let cancalable = notif.storeLogs(isGroup: false).sink(receiveCompletion: { _ in
            eNope.fulfill()
        }) { _log in
            defer { e.fulfill() }
            log = _log
        }
        defer { cancalable.cancel() }

        let person = Person()
        person.name = "John"
        try! realm.write {
            realm.add(person)
        }
        wait(for: [e, eNope], timeout: 0.2)

        let patches = realm.objects(Patch.self)
        XCTAssertEqual(patches.count, 2)
        guard let c = patches.pending(.create).first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c.objectClassName, "Person")
        XCTAssertEqual(c.objectId, person.id)
        XCTAssertEqual(c.attributes.count, 1)
        XCTAssertEqual(c.attributes.first?.key, "group")
        XCTAssertNil(c.attributes.first?.value)
        guard let m = patches.pending(.update).first else {
            XCTFail()
            return
        }
        XCTAssertEqual(m.objectClassName, "Person")
        XCTAssertEqual(m.objectId, person.id)
        XCTAssertEqual(m.attributes.count, 2)
        let attributeNames = Set(m.attributes.map { $0.key })
        XCTAssertEqual(attributeNames, Set(["group", "name"]))
        guard let realmLog = log?.log else {
            XCTFail()
            return
        }
        XCTAssertEqual(realmLog.creations.count, 1)
        XCTAssertEqual(realmLog.modifications.count, 1)
        XCTAssertEqual(realmLog.creations.first?.attributes.count, 1)
        guard let mattrs = realmLog.modifications.first?.attributes.asDictionary(key: \.key) else {
            XCTFail()
            return
        }
        
        guard case let .string(name) = try! globalScheme.getValue(for: "name", from: mattrs["name"]!.value, in: "Person") else {
            XCTFail()
            return
        }
        XCTAssertEqual(name, "John")
    }
    
    func testCreateModifyDeleteSameObject() {
        let realm = try! tryGetRealm()
        let e = self.expectation(description: "Successful completion")
        let eRealmFailure = expectation(description: "Realm failure")
        e.isInverted = true
        eRealmFailure.isInverted = true
        let notif = MeerkatRealmNotificator(objects: [Person.self])

        let cancalable = notif.storeLogs(isGroup: false).sink(receiveCompletion: { _ in
            eRealmFailure.fulfill()
        }) { log in
            print(log)
            e.fulfill()
        }
        defer { cancalable.cancel() }

        let person = Person()
        person.name = "Mark"
        try! realm.write {
            realm.add(person)
        }
        guard let realmPerson1 = realm.objects(Person.self).filter("name = 'Mark'").first else {
            XCTFail()
            return
        }
        try! realm.write {
            realmPerson1.name = "Thomas"
        }

        guard let realmPerson2 = realm.objects(Person.self).filter("name = 'Thomas'").first else {
            XCTFail()
            return
        }

        try! realm.write {
            realmPerson2.isDeleted = true
        }

        wait(for: [e, eRealmFailure], timeout: 0.3)

        let patches = realm.objects(Patch.self)
        XCTAssertEqual(patches.count, 0)
    }

    func testMobilePersonCreate() {
        let realm = try! tryGetRealm()
        let eRealmFailure = expectation(description: "Realm failure")
        eRealmFailure.isInverted = true
        let e = self.expectation(description: "Successful completion")
        let notif = MeerkatRealmNotificator(objects: [MobilePerson.self, Mobile.self])

        let cancalable = notif.storeLogs(isGroup: false).sink(receiveCompletion: { _ in
            eRealmFailure.fulfill()
        })  { log in
            print(log)
            e.fulfill()
        }
        defer { cancalable.cancel() }

        let person = MobilePerson()
        person.name = "Bob"
        try! realm.write {
            realm.add(person)
        }
        wait(for: [e, eRealmFailure], timeout: 0.3)

        let patches = realm.objects(Patch.self)
        XCTAssertEqual(patches.count, 2)
        guard let c = patches.pending(.create).first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c.objectClassName, "MobilePerson")
        XCTAssertEqual(c.objectId, person.id)
        XCTAssertEqual(c.attributes.count, 1)
        XCTAssertEqual(c.attributes.first?.key, "group")
        XCTAssertNil(c.attributes.first?.value)
        guard let m = patches.pending(.update).first else {
            XCTFail()
            return
        }
        XCTAssertEqual(m.objectClassName, "MobilePerson")
        XCTAssertEqual(m.objectId, person.id)
        XCTAssertEqual(m.attributes.count, 3)
        let attributeNames = Set(m.attributes.map { $0.key })
        XCTAssertEqual(attributeNames, Set(["group", "name", "mobile"]))

        let attributes = m.attributes.asDictionary(key: \.key)
        XCTAssertNotNil(attributes["mobile"])
        XCTAssertNil(attributes["mobile"]?.value)
    }

    func testMobilePersonMobileCreate() {
        let realm = try! tryGetRealm()
        let eRealmFailure = expectation(description: "Realm failure")
        eRealmFailure.isInverted = true
        let e = self.expectation(description: "Successful completion")
        e.expectedFulfillmentCount = 2
        let notif = MeerkatRealmNotificator(objects: [MobilePerson.self, Mobile.self])

        var logs = [SyncLog]()

        let cancalable = notif.storeLogs(isGroup: false).sink(receiveCompletion: { e in
            switch e {
            case .finished:
                print("finished")
            case .failure(let err):
                print(err)
                eRealmFailure.fulfill()
            }
        })  { log in
            logs.append(log)
            print(log)
            e.fulfill()
        }
        defer { cancalable.cancel() }

        let person = MobilePerson()
        person.name = "Lucy"
        try! realm.write {
            realm.add(person)
        }
        let mobile = Mobile()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            mobile.number = 42
            try! realm.write {
                realm.add(mobile)
                person.mobile = mobile
            }
        }
        wait(for: [e, eRealmFailure], timeout: 1)

        let patches = realm.objects(Patch.self)
        XCTAssertEqual(patches.count, 5)
        guard let c1 = patches.pending(.create).filter("objectClassName = 'MobilePerson'").first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c1.objectClassName, "MobilePerson")
        XCTAssertEqual(c1.objectId, person.id)
        XCTAssertEqual(c1.attributes.count, 1)
        XCTAssertEqual(c1.attributes.first?.key, "group")
        XCTAssertNil(c1.attributes.first?.value)

        guard let c2 = patches.pending(.create).filter("objectClassName = 'Mobile'").first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c2.objectClassName, "Mobile")
        XCTAssertEqual(c2.objectId, mobile.id)
        XCTAssertEqual(c2.attributes.count, 1)
        XCTAssertEqual(c2.attributes.first?.key, "group")
        XCTAssertNil(c2.attributes.first?.value)

        guard let m = patches.pending(.update).filter("objectClassName = 'MobilePerson'").last else {
            XCTFail()
            return
        }
        XCTAssertEqual(m.objectClassName, "MobilePerson")
        XCTAssertEqual(m.objectId, person.id)
        XCTAssertEqual(m.attributes.count, 3)
        let attributeNames = Set(m.attributes.map { $0.key })
        XCTAssertEqual(attributeNames, Set(["group", "name", "mobile"]))

        let attributes = m.attributes.asDictionary(key: \.key)
        XCTAssertNotNil(attributes["mobile"])
        XCTAssertNotNil(attributes["mobile"]?.value)
        guard let _mid = attributes["mobile"]?.value, let mid = try? globalScheme.getValue(for: "mobile", from: _mid, in: "MobilePerson") else {
            XCTFail()
            return
        }
        guard case let .object(id) = mid else {
            XCTFail()
            return
        }
        XCTAssertEqual(id, mobile.id)
        
        guard logs.count == 2 else {
            XCTFail()
            return
        }
        guard let log1 = logs[0].log else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(log1.deletions.count, 0)
        XCTAssertEqual(log1.creations.count, 1)
        XCTAssertEqual(log1.modifications.count, 1)
        print(log1)
        guard let log2 = logs[1].log else {
            XCTFail()
            return
        }
        XCTAssertEqual(log2.deletions.count, 0)
        XCTAssertEqual(log2.creations.count, 1)
        XCTAssertEqual(log2.modifications.count, 2)
        print(log2)
    }

    func testDogPersonDog() {
        let realm = try! tryGetRealm()
        let eRealmFailure = expectation(description: "Realm failure")
        eRealmFailure.isInverted = true
        let e = self.expectation(description: "Successful completion")
        let notif = MeerkatRealmNotificator(objects: [DogPerson.self, Dog.self])
        
        var logs = [SyncLog]()
        let cancalable = notif.storeLogs(isGroup: false).sink(receiveCompletion: { e in
            switch e {
            case .finished:
                print("finished")
            case .failure(let err):
                print(err)
                eRealmFailure.fulfill()
            }
        })  { log in
            logs.append(log)
            print(log)
            e.fulfill()
        }
        defer { cancalable.cancel() }

        let person = DogPerson()
        person.name = "Jenny"
        try! realm.write {
            realm.add(person)
        }
        let dog = Dog()
        dog.name = "Tobby"
        try! realm.write {
            realm.add(dog)
            person.dogs.append(dog)
        }
        wait(for: [e, eRealmFailure], timeout: 1)

        let patches = realm.objects(Patch.self)
        XCTAssertEqual(patches.count, 4)
        guard let c1 = patches.pending(.create).filter("objectClassName = 'DogPerson'").first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c1.objectClassName, "DogPerson")
        XCTAssertEqual(c1.objectId, person.id)
        XCTAssertEqual(c1.attributes.count, 1)
        XCTAssertEqual(c1.attributes.first?.key, "group")
        XCTAssertEqual(c1.attributes.first?.value, nil)

        guard let c2 = patches.pending(.create).filter("objectClassName = 'Dog'").first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c2.objectClassName, "Dog")
        XCTAssertEqual(c2.objectId, dog.id)
        XCTAssertEqual(c2.attributes.count, 1)
        XCTAssertEqual(c2.attributes.first?.key, "group")
        XCTAssertNil(c2.attributes.first?.value)

        guard let m = patches.pending(.update).first else {
            XCTFail()
            return
        }
        XCTAssertEqual(m.objectClassName, "DogPerson")
        XCTAssertEqual(m.objectId, person.id)
        XCTAssertEqual(m.attributes.count, 3)
        let attributeNames = Set(m.attributes.map { $0.key })
        XCTAssertEqual(attributeNames, Set(["group", "name", "dogs"]))

        let attributes = m.attributes.asDictionary(key: \.key)
        XCTAssertNotNil(attributes["dogs"])
        XCTAssertNotNil(attributes["dogs"]?.value)
        guard let _mid = attributes["dogs"]?.value, let mid = try? globalScheme.getValue(for: "dogs", from: _mid, in: "DogPerson") else {
            XCTFail()
            return
        }
        guard case let .array(ids) = mid else {
            XCTFail()
            return
        }
        XCTAssertEqual(ids, [dog.id])
    }

    func testTranformLog() {
        let realm = try! tryGetRealm()
        let eRealmFailure = expectation(description: "Realm failure")
        eRealmFailure.isInverted = true
        let e = self.expectation(description: "Successful completion")
        let notif = MeerkatRealmNotificator(objects: [Person.self])

        var logs = [MeerkatRTransaction]()
//        let g = SyncGroup()
//        g.id = "DEFAULT"
//        try! realm.write {
//            realm.add(g)
//        }
        let cancalable = notif.storeLogs(isGroup: false)
            .getLog().setGroups().mapToPushTransaction()
//            .mapPushTransaction(defaultGroup: "DEFAULT")
            .sink(receiveCompletion: { e in
                switch e {
                case .finished:
                    print("finished")
                case .failure(let err):
                    print(err)
                    eRealmFailure.fulfill()
                }
            })  { log in
                logs.append(log.transaction)
                print(log)
                e.fulfill()
        }
        defer { cancalable.cancel() }
        let person = Person()
        person.name = "John"
        try! realm.write {
            realm.add(person)
        }
        wait(for: [e, eRealmFailure], timeout: 0.2)
        XCTAssertEqual(logs.count, 1)
        guard let log = logs.first else {
            XCTFail()
            return
        }
        XCTAssertTrue(log.deletions.isEmpty)
        XCTAssertEqual(log.creations.count, 1)
        XCTAssertNotNil(log.creations.first)
        XCTAssertEqual(log.creations.first?.groupID, SyncGroupDefaults.defaultName)
        XCTAssertTrue(log.creations.first?.object.updates.isEmpty ?? false)
        XCTAssertEqual(log.modifications.count, 1)
        XCTAssertNotNil(log.modifications.first)
        XCTAssertTrue(log.modifications.first?.updates.filter { $0.attribute == "group" }.isEmpty ?? false)
    }

    func testGetStoredPatches() {
        sleep(1)
        let realm = getRealm()!
        let e = self.expectation(description: "Successful completion")
        let d = Date() - 1520
        try! realm.write {
            let p = Person()
            p.id = "o1"
            realm.add(p)
            let patch1 = Patch()
            let attr1 = Attribute()
            attr1.key = "group"
            attr1.value = nil
            realm.add(attr1)
            patch1.attributes.append(attr1)
            patch1.created = d
            patch1.inSync = true
            patch1.updateType = LocalUpdateType.create.rawValue
            patch1.objectClassName = "Person"
            patch1.objectId = "o1"
            realm.add(patch1)
            let log = Log()
            log.creations.append(patch1)
            log.id = "logID"
            realm.add(log)
        }

        let publisher = MeerkatClientDBWrapper(objects: [Person.self], onError: { _ in XCTFail() }).dbPublisher()
        var p: LogCodablePush? = nil
        let cancalable = publisher.sink(receiveCompletion: { _ in XCTFail() }) { (msg) in
            switch msg {
            case .push(let push):
                defer { e.fulfill() }
                p = push
            case .group:
                XCTFail()
            case .pull:
                XCTFail()
            }
        }
        defer { cancalable.cancel() }
        wait(for: [e], timeout: 1)
        guard let push = p else {
            XCTFail()
            return
        }

        XCTAssertNotEqual(push.syncLogId, "logID")
        XCTAssertEqual(push.push.groups.count, 1)
        XCTAssertEqual(push.push.transaction.deletions.count, 0)
        XCTAssertEqual(push.push.transaction.modifications.count, 0)
        XCTAssertEqual(push.push.transaction.creations.count, 1)

        guard let c = push.push.transaction.creations.first else {
            XCTFail()
            return
        }
        XCTAssertEqual(c.groupID, SyncGroupDefaults.defaultName)
        XCTAssertEqual(c.object.className, "Person")
        XCTAssertEqual(c.object.objectId, "o1")
        XCTAssertEqual(c.object.updates.count, 0)
        XCTAssertEqual(Int(c.object.timestamp.timeIntervalSince1970 * 1000), Int(d.timeIntervalSince1970 * 1000))
    }

    func testPull() {
        let e3 = self.expectation(description: "Successful completion")
        let e1 = self.expectation(description: "Request called")
        e1.isInverted = true
        let publisher = MeerkatClientDBWrapper(objects: [Person.self], onError: { _ in XCTFail() }).dbPublisher()
        var p: CodablePull? = nil
        let cancalable = publisher.sink(receiveCompletion: { _ in XCTFail() }) { (msg) in
            switch msg {
            case .push:
                XCTFail()
            case .group:
                XCTFail()
            case let .pull(_, pull):
                defer { e3.fulfill() }
                p = pull
            }
        }
        defer { cancalable.cancel() }
        let s = SyncPullRequest.wrapFetch { _ in
            e1.fulfill()
        }
        s(.newData)
        wait(for: [e1, e3], timeout: 0.1)
        XCTAssertNotNil(p)
        XCTAssertEqual(p?.groups.count, 1)
    }

    func testPullSubscriber1() {
        let realm = getRealm()!
        let g = SyncGroup()
        g.id = SyncGroupDefaults.defaultName
        try! realm.write {
            realm.add(g)
        }
        let e1 = self.expectation(description: "Request called")
        let e2 = self.expectation(description: "Request called with incorrect result")
        e2.isInverted = true
        let s = SyncPullRequest { r in
            guard r == .newData else { return e2.fulfill() }
            e1.fulfill()
        }
        s.complete(.noData, by: .user)
        let pullMsg: DownstreamMessage = .diff(.init(syncLogId: "id", diff: .init(transactions: [], groups: [])), pullRequest: s)
        let publisher = Just(pullMsg).setFailureType(to: Error.self)
        let db = MeerkatClientDBWrapper(objects: [Person.self], onError: { _ in })

        let canc = db.dbSubscriber()
        defer { canc.cancel() }
        publisher.subscribe(canc)
        wait(for: [e1, e2], timeout: 0.1)
    }

    func testPullSubscriber2() {
        let e1 = self.expectation(description: "Request called")
        e1.isInverted = true
        let s = SyncPullRequest { r in
            guard r == .failed else { return }
            e1.fulfill()
        }
        s.complete(.noData, by: .user)
        let pullMsg: DownstreamMessage = .diff(.init(syncLogId: "id", diff: .init(transactions: [], groups: [])), pullRequest: s)
        let publisher = Just(pullMsg).setFailureType(to: Error.self)
        let db = MeerkatClientDBWrapper(objects: [Person.self], onError: { _ in })

        let canc = db.dbSubscriber()
        defer { canc.cancel() }
        publisher.subscribe(canc)
        wait(for: [e1], timeout: 0.1)
    }

    func testCache() throws {
        let obj = Person()
        obj.name = "Mark"
        let id = obj.id

        let realm = getRealm()!
        try realm.write {
            realm.add(obj)
        }
        let obs = obj.observableObject()
        let d1 = self.expectation(description: "delete 1")
        let d2 = self.expectation(description: "delete 2")
        let d3 = self.expectation(description: "delete 3")
        let d6 = self.expectation(description: "delete 6")
        let d7 = self.expectation(description: "delete 7")
        d6.isInverted = true
        obs.onDelete {
            d1.fulfill()
        }
        obs.onDelete {
            d2.fulfill()
        }.onDelete {
            d3.fulfill()
        }
        let o1 = obs.observableCopy.onDelete {
            d7.fulfill()
        }
        var o2: ObservableSyncObject<Person>? = obs.observableCopy.onDelete {
            d6.fulfill()
        }
        let e1 = self.expectation(description: "created")
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) {
            defer { e1.fulfill() }
            XCTAssertEqual(obs.cache.value(for: \.id), id)
            XCTAssertEqual(obs.cache.value(for: \.name), "Mark")
        }
        wait(for: [e1], timeout: 0.3)
        try realm.write {
            obj.name = "Tom"
        }
        let e2 = self.expectation(description: "updated")
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) {
            defer { e2.fulfill() }
            XCTAssertEqual(obs.cache.value(for: \.id), id)
            XCTAssertEqual(obs.cache.value(for: \.name), "Tom")
        }
        wait(for: [e2], timeout: 0.3)

        o2 = nil
        try realm.write {
            realm.delete(obj)
        }
        let e3 = self.expectation(description: "deleted")
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) {
            defer { e3.fulfill() }
            XCTAssertEqual(obs.cache.value(for: \.id), id)
            XCTAssertEqual(obs.cache.value(for: \.name), "Tom")
        }
        wait(for: [d1, d2, d3, e3, d6, d7], timeout: 0.3)
        let d4 = self.expectation(description: "delete 4")
        let d5 = self.expectation(description: "delete 5")
        d5.isInverted = true
        obs.onDelete {
            d4.fulfill()
        }.onDelete(callIfDeleted: false) {
            d5.fulfill()
        }
        wait(for: [d4, d5], timeout: 0.3)
    }

    static var allTests = [
        ("testExample", testExample),
        ("testCreateModifyDeleteSameObject", testCreateModifyDeleteSameObject),
        ("testMobilePersonCreate", testMobilePersonCreate),
        ("testMobilePersonMobileCreate", testMobilePersonMobileCreate),
        ("testDogPersonDog", testDogPersonDog),
        ("testTranformLog", testTranformLog),
        ("testGetStoredPatches", testGetStoredPatches),
        ("testPullSubscriber1", testPullSubscriber1),
        ("testPullSubscriber2", testPullSubscriber2),
        ("testCache", testCache)
    ]
}

extension List {
    func asDictionary<T: Hashable>(key: KeyPath<Element, T>) -> [T: Element] {
        var dic = [T: Element]()
        forEach { element in
            dic[element[keyPath: key]] = element
        }
        return dic
    }
}
