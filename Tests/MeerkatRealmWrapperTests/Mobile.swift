//
//  Mobile.swift
//  
//
//  Created by Filip Klembara on 18/02/2020.
//

import RealmSwift
import MeerkatRealmWrapper
import Foundation
import MeerkatCore

final class Mobile: Object, RealmSyncObject {
    @objc dynamic var group: SyncGroup? = nil
    @objc dynamic var isDeleted: Bool = false

    @objc dynamic var id: ObjectID = UUID().uuidString

    @objc dynamic var number: Int = 0

    let owner = LinkingObjects(fromType: MobilePerson.self, property: "mobile")
}

