import XCTest
@testable import MeerkatRealmWrapper
//import MeerkatClientSyncController

import Combine
import RealmSwift
import MeerkatClientCore

final class SyncTests: XCTestCase {
    override func setUp() {
        Realm.Configuration.meerkatRealmConfiguration = .init(deleteRealmIfMigrationNeeded: true)
        let realm = try! tryGetRealm()
        try! realm.write {
            realm.deleteAll()
        }
    }

    func testExample() {
//        let token = "token"
//        let syncId = "SyncID"
//        let e = expectation(description: "ok")
//
//        let realm = getRealm()!
//
////        let g = SyncGroup()
////        g.id = syncId
//        let group = SyncGroup()
//        group.id = "ajaja"
//        let p = Person()
//        p.name = "Thom"
//        p.id = "ThomID"
////        p.group = group
//        try! realm.write {
////            realm.add(g)
////            realm.add(group)
//            realm.add(p)
//        }
//            let clientConfig = ClientConfig(scheme: .http, host: "127.0.0.1", port: 8080, rootUrl: "app")
//
//        let db = MeerkatClientDBWrapper(objects: [Person.self], onError: { _ in })
//        let controller = SyncController(db: db, clientConfig: clientConfig, syncConfig: .init(authToken: token, syncToken: syncId))
//        let errorChecker = controller.errorPublisher.sink { (err) in
//            print(err)
//        }
//        defer { errorChecker.cancel() }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            //                let realm = getRealm()!
//            //                let group = realm.object(ofType: SyncGroup.self, forPrimaryKey: "ajaja")!
//            //                realm.beginWrite()
//            //                group.isDeleted = true
//            //                try! realm.commitWrite()
//            //                try! realm.commitWrite(withoutNotifying: allNotificationTokensBut(SyncGroup.self))
//            try! realm.write {
//                //                    realm.add(p)
//                //                    group.unsubscribeUser(withId: "94C155FD-0899-43E1-97BE-F68303E68866", on: realm)
//                //                    //            group.subscribeUser(withId: "94C155FD-0899-43E1-97BE-F68303E68866", as: .read, on: realm)
//                //                    //            group.subscribeUser(withId: p.id, as: .owner, on: realm)
//                //                    //            group.subscribeUser(withId: p.id, as: .owner, on: realm)
//                p.isDeleted = true
//                //                    realm.add(group)
//            }
//        }
//        //        }
//
//
//        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
//            let realm = getRealm()!
//            let patches = realm.objects(Patch.self)
//            let attr = realm.objects(Attribute.self)
//            print(patches)
//            print(attr)
//            XCTAssertEqual(patches.count, 0)
//            XCTAssertEqual(attr.count, 0)
//            let pers = realm.objects(Person.self)
//            print(pers)
//            XCTAssertEqual(pers.count, 0)
////            let group = realm.object(ofType: SyncGroup.self, forPrimaryKey: "ajaja")!
////            try! realm.write {
////                group.isDeleted = true
////            }
//        }
//
////        let person = Person()
////        person.name = "Bobby"
////        person.group = group
////        try! realm.write {
////            realm.add(person)
////        }
//
//        wait(for: [e], timeout: 5)
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
