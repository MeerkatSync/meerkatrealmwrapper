import XCTest

import MeerkatRealmWrapperTests

var tests = [XCTestCaseEntry]()
tests += MeerkatRealmWrapperTests.allTests()
tests += SyncTests.allTests()
XCTMain(tests)
