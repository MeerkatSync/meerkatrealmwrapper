//
//  onMainLoop.swift
//  
//
//  Created by Filip Klembara on 25/02/2020.
//

import Foundation

func onSyncMainThread(_ closure: () -> Void) {
    if RunLoop.current == RunLoop.main {
        closure()
    } else {
        DispatchQueue.main.sync {
            closure()
        }
    }
}
