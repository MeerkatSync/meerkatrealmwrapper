//
//  SyncGroup.swift
//  
//
//  Created by Filip Klembara on 20/02/2020.
//

import Foundation

public final class SyncGroup: SyncObject {
    @objc
    public private(set) dynamic var group: SyncGroup? = nil
    @objc
    public dynamic var isDeleted: Bool = false
    @objc
    private dynamic var _role: String = Role.owner.rawValue

    public internal(set) var role: Role {
        get {
            Role(rawValue: _role)!
        }
        set {
            _role = newValue.rawValue
        }
    }

    @objc
    public internal(set) dynamic var id = UUID().uuidString

    @objc dynamic var version: Int = 0

    public func subscribeUser(withId id: String, as role: Role, on realm: Realm) {
        guard !isDefault else {
            assertionFailure("You can't subscribe users to user's default group")
            return
        }
        let sub = UserSubscription()
        sub.name = id
        sub.role = role.rawValue
        realm.add(sub)
        toSubscribe.append(sub)
    }

    public func unsubscribeUser(withId id: String, on realm: Realm) {
        guard !isDefault else {
            assertionFailure("You can't unsubscribe users from user's default group")
            return
        }
        let subs: [UserSubscription] = toSubscribe.map { $0 }
        subs.filter { $0.name == id }.forEach { realm.delete($0) }
        let sub = UserSubscription()
        sub.name = id
        sub.role = Role.read.rawValue
        realm.add(sub)
        toUnsubscribe.append(sub)
    }

    public var isDefault: Bool { id == SyncGroupDefaults.defaultName }

    static public func defaultGroup(on realm: Realm) -> SyncGroup {
        guard let g = realm.object(ofType: SyncGroup.self, forPrimaryKey: SyncGroupDefaults.defaultName) else {
            preconditionFailure("You can't get default group before synchronization process begins")
        }
        return g
    }

    public func changeUsersRole(id: String, newRole role: Role, on realm: Realm) {
        subscribeUser(withId: id, as: role, on: realm)
    }

    let toSubscribe = List<UserSubscription>()
    let toUnsubscribe = List<UserSubscription>()

    public override class func primaryKey() -> String? {
        return "id"
    }
}

final class UserSubscription: SyncObject {
    let isDeleted: Bool = false

    let group: SyncGroup? = nil

    public override class func primaryKey() -> String? {
        return "id"
    }
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var role: String = ""
    @objc dynamic var date: Date = Date()
}
