//
//  ObservableSyncObjects.swift
//  
//
//  Created by Filip Klembara on 19/04/2020.
//

public final class ObservableSyncObjects<T: SyncObject>: ObservableObject {
    @Published public
    var elements: [SyncObjectWrapper<T>]
    private var token: NotificationToken?
    private var results: Results<T>!

    public init(empty: T.Type) {
        elements = []
    }

    public convenience init() {
        self.init(T.all)
    }

    public init<R: RealmCollection>(_ collection: R) where R.Element == T {
        results = collection.filter("isDeleted = false")
        elements = results.wrappedArray
        activateChannelsToken()
    }

    private func activateChannelsToken() {
        token = results.observe { [unowned self] e in
            switch e {
            case let .update(col, deletions, _, _):
                deletions.forEach { index in
                    ObservableObjectCallbacks.shared.finish(object: self.elements[index])
                }
                self.elements = col.wrappedArray
            default:
                break
            }
        }
    }

    deinit {
        token?.invalidate()
    }
}
