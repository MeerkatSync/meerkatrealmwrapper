//
//  ObservableSyncObject.swift
//  
//
//  Created by Filip Klembara on 19/04/2020.
//

import Foundation

public final class ObservableSyncObject<T: SyncObject>: ObservableObject, Identifiable {
    private var _element: SyncObjectWrapper<T>?
    public var element: SyncObjectWrapper<T>? { _element }
    public var unsafeElement: SyncObjectWrapper<T> { element! }

    public let observedAttributes: Observation

    private var token: NotificationToken?

    @discardableResult
    public func onDelete(callIfDeleted: Bool = true, call: @escaping () -> Void) -> Self {
        guard let e = element, !e.isInvalidated else {
            if callIfDeleted {
                call()
            }
            return self
        }
        ObservableObjectCallbacks.shared.subscribe(object: e, with: id, callback: call)
        return self
    }

    public var observableCopy: Self {
        transformWrapped { w in
            guard let wrapped = w else {
                let res = Self.init()
                res.cache = cache
                return res
            }
            return .init(object: wrapped, observation: observedAttributes)
        }
    }

    public func ifValid(do closure: (T) throws -> Void) rethrows {
        guard let e = element else {
            return
        }
        try e.ifValid(do: closure)
    }

    public func transform<U>(_ transformation: (T?) throws -> U) rethrows -> U {
        try transformWrapped { w in
            guard let wrapped = w else {
                return try transformation(nil)
            }
            return try wrapped.transform(transformation)
        }
    }

    public func transform<U>(defaultValue value: U, _ transformation: (T) throws -> U) rethrows -> U {
        try transform { obj in
            guard let r = obj else {
                return value
            }
            return try transformation(r)
        }
    }

    public func transformWrapped<U>(defaultValue value: U, _ transformation: (SyncObjectWrapper<T>) throws -> U) rethrows -> U {
        try transformWrapped { obj in
            guard let r = obj else {
                return value
            }
            return try transformation(r)
        }
    }

    public func transformWrapped<U>(_ transformation: (SyncObjectWrapper<T>?) throws -> U) rethrows -> U {
        guard let e = element else {
            return try transformation(nil)
        }
        return try transformation(e)
    }

    public var cache: ObjectCache<T>

    public init() {
        _element = nil
        observedAttributes = .all
        cache = .init()
    }

    public convenience init(object: T, observation: Observation = .all) {
        self.init(object: object.wrapped, observation: observation)
    }

    public init(object: SyncObjectWrapper<T>, observation: Observation = .all) {
        cache = .init(object: object.wrappedObject)
        if object.isInvalidated {
            _element = nil
            observedAttributes = observation
        } else {
            _element = object
            observedAttributes = observation
            activateChannelsToken()
        }
    }

    private func activateChannelsToken() {
        token = element!.wrappedObject.observe { [weak self] change in
            guard let s = self else { return }
            switch change {
            case .error:
                break
            case .change(let props):
                if let obj = s.element {
                    s.cache.recache(with: obj.wrappedObject)
                }
                let allLabels = Set(props.map { $0.name })
                guard !allLabels.contains("isDeleted") else {
                    fallthrough
                }
                let labels = allLabels.filter { $0 != "isDeleted" }
                guard s.observedAttributes.containsObserved(observables: labels) else { return }
                
                s.objectWillChange.send()
            case .deleted:
                if let e = s.element {
                    ObservableObjectCallbacks.shared.finish(object: e, with: s.id)
                }
                s.objectWillChange.send()
                s._element = nil
                s.token?.invalidate()
                s.token = nil
            }
        }
    }

    deinit {
        token?.invalidate()
        if let e = element {
            ObservableObjectCallbacks.shared.unsubscribe(object: e, with: id)
        }
    }
}

extension ObservableSyncObject {
    public convenience init<A>(object: T, ignore a: KeyPath<T, A>) {
        let ignored = a.propertyName
        self.init(object: object, observation: Observation.ignore([ignored]))
    }

    public convenience init<A, B>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>) {
        let ignored = Set([a.propertyName, b.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E, F>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E, F, G>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E, F, G, H>(object: T, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>, _ h: KeyPath<T, H>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName, h.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A>(object: T, observe a: KeyPath<T, A>) {
        let observed = Set([a.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>) {
        let observed = Set([a.propertyName, b.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E, F>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E, F, G>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E, F, G, H>(object: T, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>, _ h: KeyPath<T, H>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName, h.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>) {
        let ignored = a.propertyName
        self.init(object: object, observation: Observation.ignore([ignored]))
    }

    public convenience init<A, B>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>) {
        let ignored = Set([a.propertyName, b.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E, F>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E, F, G>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A, B, C, D, E, F, G, H>(object: SyncObjectWrapper<T>, ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>, _ h: KeyPath<T, H>) {
        let ignored = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName, h.propertyName])
        self.init(object: object, observation: Observation.ignore(ignored))
    }

    public convenience init<A>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>) {
        let observed = Set([a.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>) {
        let observed = Set([a.propertyName, b.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E, F>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E, F, G>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public convenience init<A, B, C, D, E, F, G, H>(object: SyncObjectWrapper<T>, observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>, _ h: KeyPath<T, H>) {
        let observed = Set([a.propertyName, b.propertyName, c.propertyName, d.propertyName, e.propertyName, f.propertyName, g.propertyName, h.propertyName])
        self.init(object: object, observation: Observation.observe(observed))
    }

    public enum Observation {
        case all
        case ignore(Set<String>)
        case observe(Set<String>)

        public func contains(_ label: String) -> Bool {
            switch self {
            case .all:
                return true
            case .ignore(let ignored):
                return !ignored.contains(label)
            case .observe(let observed):
                return observed.contains(label)
            }
        }

        public func containsObserved(observables: Set<String>) -> Bool {
            switch self {
            case .all:
                return true
            case .ignore(let ignored):
                return observables.isSubset(of: ignored)
            case .observe(let observed):
                return !observed.intersection(observables).isEmpty
            }
        }
    }
}

extension KeyPath where Root: NSObject {
    fileprivate var propertyName: String {
        NSExpression(forKeyPath: self).keyPath
    }
}
