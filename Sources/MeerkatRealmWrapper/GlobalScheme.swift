//
//  GlobalScheme.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

import Foundation

private(set) var globalScheme = MeerkatSchemeDescriptor()
private let mutex = DispatchSemaphore(value: 1)

func addNewClassToScheme(class cls: ClassAttributesDescription) {
    mutex.wait()
    defer { mutex.signal() }
    globalScheme.addClass(cls)
}
