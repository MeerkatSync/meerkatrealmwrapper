//
//  MeerkatDBPublisher.swift
//  
//
//  Created by Filip Klembara on 20/02/2020.
//

import Combine
import Foundation

func MeerkatDBPublisher<T: Publisher, U: Publisher>(objectPublisher: T, groupPublisher: U) -> AnyPublisher<UpstreamMessage, Error> where T.Output == SyncLog, U.Output == SyncLog {
    let groupPublisher = MeerkatGroupPublisher(groupNotificator: groupPublisher).map { x -> UpstreamMessage in
        switch x {
        case .add(let logId, let name):
            return .group(logId: logId, message: .add(groupId: name))
        case .remove(let logId, let name):
            return .group(logId: logId, message: .remove(groupId: name))
        case .subscribe(let logId, let userId, let role, let groupId):
            return .group(logId: logId, message: .subscribe(userId: userId, role: role, groupId: groupId))
        case let .unsubscribe(logId, userId, groupId):
            return .group(logId: logId, message: .unsubscribe(userId: userId, groupId: groupId))
        }
    }
    let objectPublisher = MeerkatObjectPublisher(objectNotificator: objectPublisher).delay(for: .milliseconds(150), scheduler: DispatchQueue.global()).map { x -> UpstreamMessage in
        .push(x)
    }
    let groupAndObject = groupPublisher.priorityMerge(lowPriority: objectPublisher)
    let pullPublisher = SyncPullRequest.publisherWithError(of: Error.self).map { req -> UpstreamMessage in .pull(request: req, pull: CodablePull(schemeVersion: 0, groups: [])) }.receive(on: DispatchQueue.global(qos: .userInitiated))
    let merged = pullPublisher.priorityMerge(lowPriority: groupAndObject)
    let filtered = merged.tryCompactMap { msg -> UpstreamMessage? in
        guard let realm = getRealm() else {
            return nil
        }
        switch msg {
        case .push(let push):
            guard let p = clearPush(push, on: realm) else {
                return nil
            }
            return .push(p)
        case .group:
            return msg
        case .pull(request: let request, _):
            return .pull(request: request, pull: CodablePull(schemeVersion: 0, groups: getGroups(on: realm)))
        }
    }
    return filtered.eraseToAnyPublisher()
}

func getGroups(on realm: Realm) -> [CodableGroup] {
    var created = false
    if realm.object(ofType: SyncGroup.self, forPrimaryKey: SyncGroupDefaults.defaultName) == nil {
        created = true
        onSyncMainThread {
            let mainRealm = getRealm()!
            mainRealm.beginWrite()
            let g = SyncGroup()
            g.id = SyncGroupDefaults.defaultName
            mainRealm.add(g)
            try! mainRealm.commitWrite(withoutNotifying: allNotificationTokens)
        }
    }

    var groups: [CodableGroup] = realm.objects(SyncGroup.self).map { CodableGroup(id: $0.id, version: UInt64($0.version)) }
    if created {
        let ids = groups.map { $0.id }
        if !ids.contains(SyncGroupDefaults.defaultName) {
            groups.append(.init(id: SyncGroupDefaults.defaultName, version: 0))
        }
    }
    return groups
}

private func clearPush(_ push: LogCodablePush, on realm: Realm) -> LogCodablePush? {
    let groups = push.push.groups.filter { group in
        realm.object(ofType: SyncGroup.self, forPrimaryKey: group.id) != nil
    }
    guard let trans = clearTransaction(push.push.transaction, on: realm, logId: push.syncLogId) else {
        return nil
    }
    return LogCodablePush(syncLogId: push.syncLogId, push: CodablePush(schemeVersion: push.push.schemeVersion, groups: groups, transaction: trans))
}

private func clearTransaction(_ trans: CodableTransaction, on realm: Realm, logId: String) -> CodableTransaction? {
    guard let log = realm.object(ofType: Log.self, forPrimaryKey: logId) else {
        return nil
    }
    let ds = trans.deletions.filter { clearSubTransaction($0, on: realm, log: log, delete: true) }
    let cs = trans.creations.filter { clearSubTransaction($0.object, on: realm, log: log) }
    let ms = trans.modifications.filter { clearSubTransaction($0, on: realm, log: log) }
    guard !cs.isEmpty || !ds.isEmpty || !ms.isEmpty else {
        try! realm.write {
            realm.delete(log, cascading: true)
        }
        return nil
    }
    return CodableTransaction(deletions: ds, creations: cs, modifications: ms)
}

private func clearSubTransaction(_ sub: CodableSubtransaction, on realm: Realm, log: Log, delete: Bool = false) -> Bool {
    func clear() {
        try! realm.write {
            let c: [Patch] = log.creations.map { $0 }
            let d: [Patch] = log.deletions.map { $0 }
            let m: [Patch] = log.modifications.map { $0 }
            let alls = c + d + m
            alls.forEach { patch in
                guard patch.objectId == sub.objectId && patch.objectClassName == sub.className else {
                    return
                }
                realm.delete(patch, cascading: true)
            }
        }
    }
    var res = false
    onSyncMainThread {
        let realm = getRealm()!
        guard let object = realm.dynamicObjects(sub.className).filter("id = %@", sub.objectId).first as? SyncObject else {
            return
        }
        guard object.group != nil else {
            realm.beginWrite()
            realm.delete(object)
            try! realm.commitWrite(withoutNotifying: allNotificationTokens)
            return
        }
        res = true
    }

    guard res else {
        clear()
        return false
    }
    return true
}

func MeerkatObjectPublisher<T: Publisher>(objectNotificator: T) -> AnyPublisher<LogCodablePush, Error> where T.Output == SyncLog {
    objectNotificator
        .getLog()
        .setGroups()
        .mapToPushTransaction()
        .asCodablePushTransaction()
        .mapError { $0 as Error }
        .eraseToAnyPublisher()
}

func MeerkatGroupPublisher<T: Publisher>(groupNotificator: T) -> AnyPublisher<GroupUpdate, Error> where T.Output == SyncLog {
    groupNotificator
        .getLog()
        .map { log -> MeerkatRTransaction in
            let c: [MeerkatRCreationDescription] = log.creations.map { subtransaction(from: $0) }.map { MeerkatRCreationDescription(groupID: "", object: $0) }
            let d: [MeerkatRSubtransaction] = log.deletions.map { subtransaction(from: $0) }
            let m: [MeerkatRSubtransaction] = log.modifications.map { subtransaction(from: $0) }
            return MeerkatRTransaction(syncLogId: log.id, deletions: d, creations: c, modifications: m)
    }
        .groupChangePriority()
        .mapError { $0 as Error }
        .eraseToAnyPublisher()
}
