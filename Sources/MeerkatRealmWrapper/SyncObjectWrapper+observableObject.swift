//
//  SyncObjectWrapper+observableObject.swift
//  
//
//  Created by Filip Klembara on 25/04/2020.
//

extension SyncObjectWrapper {
    public func observableObject() -> ObservableSyncObject<T> {
        .init(object: self)
    }

    public func observableObject<A>(ignore a: KeyPath<T, A>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a)
    }

    public func observableObject<A, B>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b)
    }

    public func observableObject<A, B, C>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c)
    }

    public func observableObject<A, B, C, D>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d)
    }

    public func observableObject<A, B, C, D, E>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e)
    }

    public func observableObject<A, B, C, D, E, F>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e, f)
    }

    public func observableObject<A, B, C, D, E, F, G>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e, f, g)
    }

    public func observableObject<A, B, C, D, E, F, G, H>(ignore a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>, _ h: KeyPath<T, H>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e, f, g, h)
    }
    public func observableObject<A>(observe a: KeyPath<T, A>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a)
    }

    public func observableObject<A, B>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b)
    }

    public func observableObject<A, B, C>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c)
    }

    public func observableObject<A, B, C, D>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d)
    }

    public func observableObject<A, B, C, D, E>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e)
    }

    public func observableObject<A, B, C, D, E, F>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e, f)
    }

    public func observableObject<A, B, C, D, E, F, G>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>) -> ObservableSyncObject<T> {
        .init(object: self, ignore: a, b, c, d, e, f, g)
    }

    public func observableObject<A, B, C, D, E, F, G, H>(observe a: KeyPath<T, A>, _ b: KeyPath<T, B>, _ c: KeyPath<T, C>, _ d: KeyPath<T, D>, _ e: KeyPath<T, E>, _ f: KeyPath<T, F>, _ g: KeyPath<T, G>, _ h: KeyPath<T, H>) -> ObservableSyncObject<T> {
        .init(object: self, observe: a, b, c, d, e, f, g, h)
    }
}
