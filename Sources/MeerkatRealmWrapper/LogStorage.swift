//
//  LogStorage.swift
//
//
//  Created by Filip Klembara on 18/02/2020.
//

import Combine
import Foundation

private let initMutex = DispatchSemaphore(value: 1)
private var initHappenedPatch = false
private var initHappenedLog = false
private var initHappenedGroup = false

final class LogStorage<Upstream: Publisher> where Upstream.Failure == MeerkatRealmNotificator.MRWError, Upstream.Output == Log {
    private var notificationTokenLog: NotificationToken? = nil
    private let upstream: Upstream
    private let isGroup: Bool
    fileprivate init(upstream: Upstream, isGroup: Bool) {
        self.isGroup = isGroup

        self.upstream = upstream
        initMutex.wait()
        defer { initMutex.signal() }

        if !initHappenedLog {
            let realm = getRealm()
            notificationTokenLog = realm?.objects(Log.self).observe { [unowned self] change in
                guard case let .initial(objects) = change else {
                    return
                }
                self.notificationTokenLog?.invalidate()
                self.notificationTokenLog = nil
                initMutex.wait()
                defer { initMutex.signal() }
                guard !initHappenedLog else { return }
                if !initHappenedLog {
                    defer { initHappenedLog = true }
                    let realm = objects.realm!
                    try! realm.write {
                        objects.realm?.delete(objects.map { $0 })
                    }
                }
            }
        }
    }

    deinit {
        self.notificationTokenLog?.invalidate()
    }
}

extension Publisher where Output == Log, Failure == MeerkatRealmNotificator.MRWError {
    func storeLogs(isGroup: Bool) -> LogStorage<Self> {
        return LogStorage(upstream: self, isGroup: isGroup)
    }
}

extension LogStorage: Publisher {
    func receive<Downstream: Subscriber>(subscriber: Downstream) where Downstream.Failure == MeerkatRealmNotificator.MRWError, Downstream.Input == SyncLog {
        upstream.subscribe(Inner(downstream: subscriber, isGroup: isGroup))
    }

    typealias Output = SyncLog

    typealias Failure = MeerkatRealmNotificator.MRWError
}

// MARK: - Inner
@available(OSX 10.15, *)
extension LogStorage {
    fileprivate final class Inner<Downsream: Subscriber> where Downsream.Failure == MeerkatRealmNotificator.MRWError, Downsream.Input == SyncLog {
        private var status = SubscriptionStatus.awaitingSubscription
        private let mutex = DispatchSemaphore(value: 1)
        private var hasLogs = false
        private let downstream: Downsream
        private var demands: Subscribers.Demand = .none
        private let predicate: String
        private var notificationToken: NotificationToken?
        init(downstream: Downsream, isGroup: Bool) {
            predicate = isGroup ? "objectClassName = 'SyncGroup'" : "objectClassName != 'SyncGroup'"
            self.downstream = downstream
            let realm = getRealm()!
            notificationToken = realm.objects(Patch.self).filter(predicate).filter("inSync = true").observe { [unowned self] change in
                guard case let .initial(objects) = change else {
                    return
                }
                self.notificationToken?.invalidate()
                self.notificationToken = nil
                initMutex.wait()
                defer { initMutex.signal() }
                guard isGroup ? !initHappenedGroup : !initHappenedPatch else {
                    return
                }
                self.mutex.wait()
                defer { self.mutex.signal() }
                if !initHappenedPatch {
                    defer {
                        if isGroup {
                            initHappenedGroup = true
                        } else {
                            initHappenedPatch = true
                        }
                    }
                    if !objects.isEmpty {
                        let realm = objects.realm!
                        try! realm.write {
                            objects.forEach {
                                $0.inSync = false
                            }
                        }
                        self.hasLogs = true
                        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + 0.1) {
                            self.mutex.wait()
                            defer { self.mutex.signal() }
                            self.sendLog()
                        }
                    }
                }
            }
        }
        deinit {
            notificationToken?.invalidate()
        }
    }
}

// MARK: - Inner + Subscription
extension LogStorage.Inner: Subscription {
    func request(_ demand: Subscribers.Demand) {
        mutex.wait()
        defer { mutex.signal() }
        self.demands += demand
        sendLog()
    }

    fileprivate func getLog() -> Result<SyncLog?, MeerkatRealmNotificator.MRWError> {
        do {
            let realm = try tryGetRealm()
            let log = try realm.write { () -> SyncLog? in
                let allPatches = realm.objects(Patch.self)
//                let inSync = allPatches.filter("inSync = true").map { $0 }
//                realm.delete(inSync)
                let notInSync = allPatches.filter(predicate).filter("inSync = false")
                let ds: [Patch] = notInSync.pending(.delete).map { $0 }
                let cs: [Patch] = notInSync.pending(.create).map { $0 }
                let ms: [Patch] = notInSync.pending(.update).map { $0 }
                (ds + cs + ms).forEach { p in
                    p.inSync = true
                }
                guard !ds.isEmpty || !cs.isEmpty || !ms.isEmpty else {
                    return nil
                }
                let log = Log()
                log.deletions.append(objectsIn: ds)
                log.creations.append(objectsIn: cs)
                log.modifications.append(objectsIn: ms)
                realm.add(log)
                return SyncLog(id: log.id)
            }
            hasLogs = false
            return .success(log)
        } catch {
            return .failure(.realmError(error))
        }
    }

    fileprivate func sendLog() {
        guard self.demands > .none else {
            return
        }
        guard hasLogs else {
            return
        }
        let logRes = getLog()

        let d: Subscribers.Demand
        switch logRes {
        case .success(let _log):
            guard let log = _log else {
                return
            }
            self.demands -= 1
            mutex.signal()
            d = downstream.receive(log)
        case .failure(let err):
        mutex.signal()
            downstream.receive(completion: .failure(err))
            mutex.wait()
            cancel()
            return
        }
        mutex.wait()
        self.demands += d
    }

    func cancel() {
        mutex.wait()
        defer { mutex.signal() }
        guard case let .subscribed(subscription) = status else {
            return
        }
        demands = .none
        subscription.cancel()
        status = .terminated
    }
}

// MARK: - Inner + Subscriber
extension LogStorage.Inner: Subscriber {
    func receive(subscription: Subscription) {
        switch status {
        case .terminated, .subscribed:
            subscription.cancel()
        case .awaitingSubscription:
            status = .subscribed(subscription: subscription)
            downstream.receive(subscription: self)
            subscription.request(.unlimited)
        }
    }

    func receive(_ input: Log) -> Subscribers.Demand {
        let res = update(deletions: input.deletions, insertions: input.creations, modifications: input.modifications)
        guard case .success = res else {
            status = .terminated
            return .none
        }
        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + 0.1) {
            self.mutex.wait()
            defer { self.mutex.signal() }
            if self.demands > .none && self.hasLogs {
                self.sendLog()
            }
        }
        return .unlimited
    }
    func receive(completion: Subscribers.Completion<MeerkatRealmNotificator.MRWError>) {
        status = .terminated
    }
}

// MARK: - Inner + Update
extension LogStorage.Inner {
    fileprivate func update(deletions: List<Patch>, insertions: List<Patch>, modifications: List<Patch>) -> Result<Void, MeerkatRealmNotificator.MRWError> {
        mutex.wait()
        defer { mutex.signal() }
        var deleted = [Patch]()
        var modified = [Patch]()
        // swiftlint:disable:next force_try
        let realm: Realm
        do {
            realm = try tryGetRealm()
        } catch {
            return .failure(.realmError(error))
        }
        let wasInTransaction = realm.isInWriteTransaction
        if !wasInTransaction {
            realm.beginWrite()
        }

        deletions.forEach { patch in
            let offPatches = realm.objects(Patch.self).filter(predicate).filter("inSync = false").by(className: patch.objectClassName, id: patch.objectId)
            let offCreate = offPatches.pending(.create)
            let offDelete = offPatches.pending(.delete)
            let isLocallyCreated = !offCreate.isEmpty
            let isLocalyDeleted = !offDelete.isEmpty
            offPatches.forEach { realm.delete($0.attributes) }
            realm.delete(offPatches)
            if !isLocallyCreated || isLocalyDeleted {
                deleted.append(patch)
            }
        }

        modifications.forEach { patch in
            let offPatches = realm.objects(Patch.self).filter(predicate).filter("inSync = false").by(className: patch.objectClassName, id: patch.objectId)
            if let updated = offPatches.pending(.update).first {
                updated.attributes.removeAll()
                updated.attributes.append(objectsIn: patch.attributes)
            } else {
                modified.append(patch)
            }
        }

        realm.add(deleted)
        realm.add(insertions)
        realm.add(modified)
        if !wasInTransaction {
            // swiftlint:disable:next force_try
            try! realm.commitWrite()
        }
        hasLogs = true
        return .success(())
    }
}
