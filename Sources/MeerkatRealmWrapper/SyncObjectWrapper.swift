//
//  SyncObjectWrapper.swift
//  
//
//  Created by Filip Klembara on 19/04/2020.
//

@dynamicMemberLookup
public struct SyncObjectWrapper<T: SyncObject>: Identifiable {
    public let id: String

    public let wrappedObject: T

    private var cache: [PartialKeyPath<T>: Any] = [:]

    public init(object: T) {
        id = object.id
        wrappedObject = object
    }

    public func ifValid(do closure: (T) throws -> Void) rethrows {
        guard !wrappedObject.isInvalidated else {
            return
        }
        try closure(wrappedObject)
    }

    public func transform<U>(_ transformation: (T?) throws -> U) rethrows -> U {
        guard !wrappedObject.isInvalidated else {
            return try transformation(nil)
        }
        return try transformation(wrappedObject)
    }

    public subscript<U>(dynamicMember keyPath: ReferenceWritableKeyPath<T, U>) -> U {
        get {
            wrappedObject[keyPath: keyPath]
        }
        nonmutating set {
            wrappedObject[keyPath: keyPath] = newValue
        }
    }

    public subscript<U>(dynamicMember keyPath: KeyPath<T, U>) -> U {
            wrappedObject[keyPath: keyPath]

    }
}

