//
//  RealmCollection+funcs.swift
//  
//
//  Created by Filip Klembara on 19/04/2020.
//

extension RealmCollection where Element: SyncObject {
    public var wrappedArray: [SyncObjectWrapper<Element>] {
        map { $0.wrapped }
    }

    public var observedObjects: ObservableSyncObjects<Element> { .init(self) }

    public func sorted<T: Comparable>(byKeyPath keyPath: KeyPath<Element, T>, ascending: Bool = true) -> Results<Element> {
        sorted(byKeyPath: NSExpression(forKeyPath: keyPath).keyPath, ascending: ascending)
    }
}
