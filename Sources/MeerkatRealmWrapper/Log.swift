//
//  Log.swift
//
//
//  Created by Filip Klembara on 18/02/2020.
//

import Foundation

final class Log: Object {
    @objc dynamic var id = UUID().uuidString
    let deletions = List<Patch>()
    let creations = List<Patch>()
    let modifications = List<Patch>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
