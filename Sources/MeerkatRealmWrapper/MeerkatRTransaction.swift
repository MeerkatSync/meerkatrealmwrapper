//
//  MeerkatRTransaction.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

import Foundation

public struct MeerkatRPush {
    public let syncLogId: String

    public let groups: [MeerkatRGroup]

    public let transaction: MeerkatRTransaction

    public init(groups: [MeerkatRGroup], transaction: MeerkatRTransaction) {
        self.syncLogId = transaction.syncLogId
        self.groups = groups
        self.transaction = transaction
    }
}

public struct MeerkatRGroup {
    public let id: String
    public let version: Int

    public init(id: String, version: Int) {
        self.id = id
        self.version = version
    }
}

public struct MeerkatRTransaction {
    public let syncLogId: String

    public let deletions: [MeerkatRSubtransaction]

    public let creations: [MeerkatRCreationDescription]

    public let modifications: [MeerkatRSubtransaction]

    public init(syncLogId: String, deletions: [MeerkatRSubtransaction], creations: [MeerkatRCreationDescription], modifications: [MeerkatRSubtransaction]) {
        self.syncLogId = syncLogId
        self.deletions = deletions
        self.creations = creations
        self.modifications = modifications
    }
}

public struct MeerkatRSubtransaction {
    public let timestamp: Date
    public let updates: [MeerkatRUpdate]
    public let className: String
    public let objectId: ObjectID

    public init(timestamp: Date, updates: [MeerkatRUpdate], className: String, objectId: ObjectID) {
        self.timestamp = timestamp
        self.updates = updates
        self.className = className
        self.objectId = objectId
    }
}

public struct MeerkatRCreationDescription {
    public let groupID: GroupID
    public let object: MeerkatRSubtransaction

    public init(groupID: GroupID, object: MeerkatRSubtransaction) {
        self.groupID = groupID
        self.object = object
    }
}

public struct MeerkatRUpdate {
    public let attribute: String
    public let value: UpdateValue

    public init(attribute: String, value: UpdateValue) {
        self.attribute = attribute
        self.value = value
    }
}

public struct MeerkatRDiffGroupUpdate {
    public let groupId: String
    public let version: GroupVersion
    public let role: Role

    public init(groupId: String, version: GroupVersion, role: Role) {
        self.groupId = groupId
        self.version = version
        self.role = role
    }
}

public struct MeerkatRDiff {
    public let syncLogId: String
    public let transaction: [MeerkatRTransaction]
    public let groups: [MeerkatRDiffGroupUpdate]

    init(syncLogId: String, transaction: [MeerkatRTransaction], groups: [MeerkatRDiffGroupUpdate]) {
        self.syncLogId = syncLogId
        self.transaction = transaction
        self.groups = groups
    }
}
