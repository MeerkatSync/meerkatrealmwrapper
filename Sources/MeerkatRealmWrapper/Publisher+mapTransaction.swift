//
//  Publisher+mapTransaction.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

import Combine

extension Publisher where Output == SyncLog {
    func getLog() -> AnyPublisher<Log, MeerkatRealmNotificator.MRWError> {
        tryCompactMap { log -> Log? in
            let realm = try tryGetRealm()
            guard let log = log.getLog(on: realm) else {
                return nil
            }
            return log
        }.hideError()
    }
}

extension Publisher where Output == (Log, [MeerkatRGroup]) {
    func mapToPushTransaction() -> AnyPublisher<MeerkatRPush, MeerkatRealmNotificator.MRWError> {
        tryMap { args -> MeerkatRPush in
            let (log, groups) = args

            let transaction = _transformToTransaction(log: log)
            let meerGroups = groups
            return MeerkatRPush(groups: meerGroups, transaction: transaction)
        }.hideError()
    }
}

extension Publisher where Output == Log {
    func mapToTransaction() -> Publishers.Map<Self, MeerkatRTransaction> {
        map { _transformToTransaction(log: $0) }
    }

    func setGroups() -> AnyPublisher<(Log, [MeerkatRGroup]), MeerkatRealmNotificator.MRWError> {
        let groups = map { log -> (Log, [MeerkatRGroup]) in
            let realm = log.realm!
            let codableGroups = getGroups(on: realm)
            let groups = codableGroups.map { MeerkatRGroup(id: $0.id, version: Int($0.version))}
            return (log, groups)
        }

        let setGroups = groups.tryMap { args -> (Log, [MeerkatRGroup]) in
            let (log, groups) = args
            let ids = Set(groups.map { $0.id })
            let all = [log.creations, log.deletions, log.modifications]
            all.forEach { $0.forEach { $0.correctGroup(groups: ids) }}

            return (log, groups)
        }
        return setGroups.hideError()
    }
}

extension Publisher {
    func hideError() -> AnyPublisher<Output, MeerkatRealmNotificator.MRWError> {
        mapError { err -> MeerkatRealmNotificator.MRWError in
            if let error = err as? MeerkatRealmNotificator.MRWError {
                return error
            }
            return MeerkatRealmNotificator.MRWError.realmError(err)
        }.eraseToAnyPublisher()
    }
}

private func _transformToTransaction(log: Log) -> MeerkatRTransaction {
    let deletions: [MeerkatRSubtransaction] = log.deletions.map { subtransaction(from: $0) }
    let modifications: [MeerkatRSubtransaction] = log.modifications.map { subtransaction(from: $0) }
    let creations: [MeerkatRCreationDescription] = log.creations.map { creationDescription(from: $0) }
    let transaction = MeerkatRTransaction(syncLogId: log.id,
                                          deletions: deletions,
                                          creations: creations,
                                          modifications: modifications)
    return transaction
}

private func creationDescription(from patch: Patch) -> MeerkatRCreationDescription {
    let groupNameAttr = patch.attributes.filter("key = 'group'").first!
    guard case let .object(groupName) = try! globalScheme.getValue(for: groupNameAttr.key, from: groupNameAttr.value, in: patch.objectClassName) else {
        preconditionFailure()
    }
    return MeerkatRCreationDescription(groupID: groupName, object: subtransaction(from: patch))
}

func subtransaction(from patch: Patch) -> MeerkatRSubtransaction {
    let updates: [MeerkatRUpdate] = patch.attributes.filter("key != 'group'").map { attr -> MeerkatRUpdate in
        MeerkatRUpdate(attribute: attr.key, value: try! globalScheme.getValue(for: attr.key, from: attr.value, in: patch.objectClassName))
    }
    return MeerkatRSubtransaction(
        timestamp: patch.created,
        updates: updates,
        className: patch.objectClassName,
        objectId: patch.objectId)
}
