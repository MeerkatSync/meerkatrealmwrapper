//
//  PriorityStreamMerge.swift
//  
//
//  Created by Filip Klembara on 24/02/2020.
//

import Combine
import Foundation

extension Publisher {
    public func priorityMerge<LowPriority: Publisher>(lowPriority: LowPriority) -> Publishers.PriorityStreamMerge<Self, LowPriority> {
        Publishers.PriorityStreamMerge(highPriority: self, lowPriority: lowPriority)
    }
}

extension Publishers {
    public final class PriorityStreamMerge<HPUpstream: Publisher, LPUpstream: Publisher> where LPUpstream.Output == HPUpstream.Output, LPUpstream.Failure == HPUpstream.Failure {
        private let highPriority: HPUpstream
        private let lowPriority: LPUpstream
        public init(highPriority: HPUpstream, lowPriority: LPUpstream) {
            self.highPriority = highPriority
            self.lowPriority = lowPriority
        }
    }
}

extension Publishers.PriorityStreamMerge: Publisher {
    public func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
        let inner = Inner(downstream: subscriber)
        highPriority.map{ x -> Inner<S>.In in .hp(x) }.subscribe(inner)
        lowPriority.map { x -> Inner<S>.In in .lp(x) }.subscribe(inner)
    }

    public typealias Output = HPUpstream.Output

    public typealias Failure = HPUpstream.Failure
}

extension Publishers.PriorityStreamMerge {

    fileprivate final class Inner<Downstream: Subscriber> {
        enum Status {
            case terminated
            case awaitingSubscription
            case subscribedOne(subscription: Subscription)
            case subscribed(one: Subscription, two: Subscription)
        }
        enum In {
            case hp(Downstream.Input)
            case lp(Downstream.Input)
        }
        enum Inputs {
            case none
            case one(In)
            case both(hp: Downstream.Input, lp: Downstream.Input)
        }
        enum RequestStatusWaiting {
            case none
            case lowPriority
            case highPriority
            case both
        }
        private let downstream: Downstream
        private var status = Status.awaitingSubscription
        private let mutex = DispatchSemaphore(value: 1)
        private var demands: Subscribers.Demand = .none
        private var inputs: Inputs = .none
        private var requestStatus = RequestStatusWaiting.none
        init(downstream: Downstream) {
            self.downstream = downstream
        }
    }
}

extension Publishers.PriorityStreamMerge.Inner: Subscription {
    @discardableResult
    private func mutexFree<T>(block: () -> T) -> T {
        mutex.signal()
        let r = block()
        mutex.wait()
        return r
    }

    func request(_ demand: Subscribers.Demand) {
        mutex.wait()
        defer { mutex.signal() }
        demands += demand
        guard demands > .none else {
            return
        }
        switch status {
        case .terminated, .awaitingSubscription:
            return
        case .subscribedOne(let subscription):
            switch inputs {
            case .none:
                switch requestStatus {
                case .none:
                    requestStatus = .highPriority
                    mutexFree {
                        subscription.request(.max(1))
                    }
                case .highPriority:
                    break
                default:
                    preconditionFailure()
                }
            default:
                break
            }
        case .subscribed(let one, let two):
            switch inputs {
            case .none:
                switch requestStatus {
                case .none:
                    requestStatus = .both
                    mutexFree {
                        one.request(.max(1))
                        two.request(.max(1))
                    }
                case .lowPriority:
                    requestStatus = .both
                    mutexFree {
                        one.request(.max(1))
                    }
                case .highPriority:
                    requestStatus = .both
                    mutexFree {
                        two.request(.max(1))
                    }
                case .both:
                    break
                }
            case .one(let inp):
                switch inp {
                case .hp:
                    switch requestStatus {
                    case .none:
                        requestStatus = .lowPriority
                        mutexFree {
                            two.request(.max(1))
                        }
                    case .lowPriority:
                        break
                    case .highPriority:
                        preconditionFailure()
                    case .both:
                        preconditionFailure()
                    }
                case .lp:
                    switch requestStatus {
                    case .none:
                        requestStatus = .highPriority
                        mutexFree {
                            one.request(.max(1))
                        }
                    case .lowPriority:
                        preconditionFailure()
                    case .highPriority:
                        break
                    case .both:
                        preconditionFailure()
                    }
                }
            case .both:
                assert(requestStatus == .none)
                break
            }
        }
        lockedTrySend()
    }

    private func lockedTrySend() {
        guard demands > .none else {
            return
        }
        switch status {
        case .terminated, .awaitingSubscription:
            return
        case .subscribedOne, .subscribed:
            switch inputs {
            case .none:
                return
            case .one(let inp):
                switch inp {
                case .hp(let i), .lp(let i):
                    assert(demands > .none)
                    inputs = .none
                    let d = mutexFree {
                        downstream.receive(i)
                    }
                    guard !isTerminated else { return }
                    assert(demands > .none)
                    demands -= 1
                    mutexFree { request(d) }
                }
            case .both(hp: let hp, lp: let lp):
                assert(demands > .none)
                inputs = .one(.lp(lp))
                let d = mutexFree {
                    downstream.receive(hp)
                }
                guard !isTerminated else { return }
                assert(demands > .none)
                demands -= 1
                mutexFree { request(d) }
            }
        }
    }

    private var isTerminated: Bool {
        guard case .terminated = status else {
            return false
        }
        return true
    }

    func cancel() {
        mutex.wait()
        defer { mutex.signal() }
        inputs = .none
        demands = .none
        switch status {
        case .terminated, .awaitingSubscription:
            status = .terminated
            return
        case .subscribedOne(let s):
            s.cancel()
        case .subscribed(let one, let two):
            one.cancel()
            two.cancel()
        }
        status = .terminated
    }
}

extension Publishers.PriorityStreamMerge.Inner: Subscriber {

    func receive(subscription: Subscription) {
        mutex.wait()
        defer { mutex.signal() }
        switch status {
        case .terminated, .subscribed:
            subscription.cancel()
            return
        case .awaitingSubscription:
            status = .subscribedOne(subscription: subscription)
            mutexFree {
                downstream.receive(subscription: self)
            }
        case .subscribedOne(let one):
            status = .subscribed(one: one, two: subscription)
            if requestStatus == .highPriority && demands > .none {
                guard case .none = inputs else {
                    return
                }
                requestStatus = .both
                mutexFree {
                    subscription.request(.max(1))
                }
            }
        }
    }

    func receive(_ input: Publishers.PriorityStreamMerge<HPUpstream, LPUpstream>.Inner<Downstream>.In) -> Subscribers.Demand {
        mutex.wait()
        defer { mutex.signal() }
        switch status {
        case .terminated, .awaitingSubscription:
            return .none
        case .subscribedOne:
            switch requestStatus {
            case .none, .lowPriority, .both:
                preconditionFailure()
            case .highPriority:
                requestStatus = .none
            }
            switch inputs {
            case .none:
                switch input {
                case .hp:
                    inputs = .one(input)
                case .lp:
                    preconditionFailure()
                }
            case .one, .both:
                preconditionFailure()
            }
        case .subscribed:
            let isHighPriorityInput: Bool
            switch input {
            case .hp:
                isHighPriorityInput = true
            case .lp:
                isHighPriorityInput = false
            }
            switch requestStatus {
            case .none:
                preconditionFailure()
            case .lowPriority:
                precondition(!isHighPriorityInput)
                requestStatus = .none
            case .highPriority:
                precondition(isHighPriorityInput)
                requestStatus = .none
            case .both:
                requestStatus = isHighPriorityInput ? .lowPriority : .highPriority
            }
            switch inputs {
            case .none:
                inputs = .one(input)
            case .one(let inp):
                switch inp {
                case .hp(let i):
                    switch input {
                    case .hp:
                        preconditionFailure()
                    case .lp(let i2):
                        inputs = .both(hp: i, lp: i2)
                    }
                case .lp(let i):
                    switch input {
                    case .lp:
                        preconditionFailure()
                    case .hp(let i2):
                        inputs = .both(hp: i2, lp: i)
                    }
                }
            case .both:
                preconditionFailure()
            }
        }
        lockedTrySend()
        return .none
    }

    func receive(completion: Subscribers.Completion<Downstream.Failure>) {
        mutex.wait()
        switch status {
        case .terminated, .awaitingSubscription:
            mutex.signal()
            return
        case .subscribedOne, .subscribed:
            mutex.signal()
            cancel()
            downstream.receive(completion: completion)
        }
    }

    typealias Input = In

    typealias Failure = Downstream.Failure
}
