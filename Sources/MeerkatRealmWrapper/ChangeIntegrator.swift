//
//  ChangeIntegrator.swift
//  
//
//  Created by Filip Klembara on 24/02/2020.
//

import Foundation
import Combine

extension Subscribers {
    public final class ChangeIntegrator<Failure: Error> {
        private var status = SubscriptionStatus.awaitingSubscription
        private var mutex = DispatchSemaphore(value: 1)
        private let onError: (Failure) -> Void
        private var currentMessage: DownstreamMessage? {
            didSet {
                assert(mutex.wait(timeout: .now()) == .timedOut)
                guard let msg = currentMessage else {
                    guard case .subscribed(let s) = status else {
                        return
                    }
                    mutex.signal()
                    s.request(.max(1))
                    mutex.wait()
                    return
                }
                integrate(msg: msg)
            }
        }
        public init(onError: @escaping (Failure) -> Void) {
            self.onError = onError
        }
    }
}

extension Subscribers.ChangeIntegrator: Subscriber {
    public func receive(subscription: Subscription) {
        mutex.wait()
        guard case .awaitingSubscription = status else {
            mutex.signal()
            subscription.cancel()
            return
        }
        status = .subscribed(subscription: subscription)
        mutex.signal()
        subscription.request(.max(1))
    }

    public func receive(_ input: DownstreamMessage) -> Subscribers.Demand {
        mutex.wait()
        defer { mutex.signal() }
        assert(currentMessage == nil)
        currentMessage = input
        return .none
    }

    public func receive(completion: Subscribers.Completion<Failure>) {
        mutex.wait()
        guard case .subscribed = status else {
            mutex.signal()
            return
        }
        mutex.signal()
        cancel()
        mutex.signal()
        switch completion {
        case .finished:
            break
        case .failure(let err):
            onError(err)
        }

    }

    public typealias Input = DownstreamMessage
}

extension Subscribers.ChangeIntegrator: Cancellable {
    public func cancel() {
        mutex.wait()
        guard case .subscribed(let subscription) = status else {
            status = .terminated
            mutex.signal()
            return
        }
        status = .terminated
        mutex.signal()
        subscription.cancel()
    }
}

extension Subscribers.ChangeIntegrator {

    private func integrateDiffGroups(_ remoteGroups: [CodableDiffGroupUpdate], on realm: Realm) -> (groups: [String: SyncGroup], newData: Bool) {
        let localGroups: [SyncGroup] = realm.objects(SyncGroup.self).filter("isDeleted = false").map { $0 }.filter { g in
            realm.objects(Patch.self).filter("inSync = false").filter("objectClassName = 'SyncGroup' AND objectId = %@", g.id).pending(.create).isEmpty
        }
        var newData = false
        let remoteGroupIds = Set(remoteGroups.map { $0.groupId })
        let onlyLocalGroups = localGroups.filter { !remoteGroupIds.contains($0.id) }
        let localGroupsIds = Set(localGroups.map { $0.id })
        let sharedGroups = localGroups.filter { remoteGroupIds.contains($0.id) }
        let remoteSharedGroups = remoteGroups.filter { localGroupsIds.contains($0.groupId) }
        if !onlyLocalGroups.isEmpty { newData = true }
        onlyLocalGroups.forEach { g in
            allObservingObjects.forEach { t in
                let className = t.className()
                let obj = realm.dynamicObjects(className).filter("group.id = %@", g.id)
                realm.delete(obj)
            }
            realm.delete(g)
        }
        let onlyRemoteGroups = remoteGroups.filter { !localGroupsIds.contains($0.groupId) }
        let newGroups = onlyRemoteGroups.map { g -> SyncGroup in
            let group = SyncGroup()
            group.id = g.groupId
            group.role = g.role
            group.version = Int(g.version)
            realm.add(group)
            return group
        }
        if !newGroups.isEmpty { newData = true }
        let groupAccess = { () -> Dictionary<String, SyncGroup> in
            var dic = Dictionary<String, SyncGroup>()
            newGroups.forEach { g in
                dic[g.id] = g
            }
            sharedGroups.forEach { g in
                dic[g.id] = g
            }
            return dic
        }()
        remoteSharedGroups.forEach { g in
            let group = groupAccess[g.groupId]!
            if group.role != g.role {
                newData = true
                group.role = g.role
            }
            let newVersion = g.version >= 0 ? Int(g.version) : 0
            if group.version != newVersion {
                group.version = Int(g.version)
                newData = true
            }
        }
        return (groupAccess, newData)
    }

    private func integrateDiff(_ diff: LogCodableDiff, with pullRequest: SyncPullRequest?, on realm: Realm) {
        let groupIntegrationResult = integrateDiffGroups(diff.diff.groups, on: realm)
        let validGroups = groupIntegrationResult.groups
        var newData = groupIntegrationResult.newData
        defer { pullRequest?.complete(newData ? .newData : .noData, by: .meerkat) }
        let transactions = diff.diff.transactions
        let allDeletions = transactions.flatMap { $0.deletions }
        if let log = realm.object(ofType: Log.self, forPrimaryKey: diff.syncLogId) {
            let dels = log.deletions
            dels.forEach { patch in
                guard let obj = realm.dynamicObjects(patch.objectClassName).filter("id = %@", patch.objectId).first else {
                    return
                }
                realm.delete(obj)
            }
        }
        if !allDeletions.isEmpty { newData = true }
        allDeletions.forEach { deletion in
            guard let obj = realm.dynamicObjects(deletion.className).filter("id = %@", deletion.objectId).first else {
                return
            }
            realm.delete(obj)
        }
        let allCreations = transactions.flatMap { $0.creations }
        if !allCreations.isEmpty { newData = true }
        allCreations.forEach { creation in
            guard let group = validGroups[creation.groupID] else {
                return
            }
            let object = realm.dynamicCreate(creation.object.className) as! SyncObject
            object["id"] = creation.object.objectId
            object["group"] = group
            for c in creation.object.updates {
                guard let attr = try? object.attributeValue(for: c.attribute, from: c.value) else {
                    continue
                }
                object.setAttribute(c.attribute, to: attr)
            }
            let patches = realm.objects(Patch.self).filter("inSync = false").by(className: creation.object.className, id: creation.object.objectId)
            let cs = patches.pending(.create)
            let ms = patches.pending(.update)
            realm.delete(cs, cascading: true)
            realm.delete(ms, cascading: true)
        }
        let allModificaitions = transactions.flatMap { $0.modifications }
        if !allModificaitions.isEmpty { newData = true }
        allModificaitions.forEach { modif in
            let allPatches = realm.objects(Patch.self).filter("inSync = false").by(className: modif.className, id: modif.objectId)
            let isDeleted = !allPatches.pending(.delete).isEmpty
            guard !isDeleted else {
                return
            }
            let allMs = allPatches.pending(.update)
            let older = allMs.filter("created < %@", modif.timestamp)
            realm.delete(older, cascading: true)
            if let obj = realm.dynamicObjects(modif.className).filter("id = %@", modif.objectId).first as? SyncObject {
                let cs = allPatches.pending(.create)
                realm.delete(cs, cascading: true)
                modif.updates.forEach { attr in
                    guard let value = try? obj.attributeValue(for: attr.attribute, from: attr.value) else {
                        return
                    }
                    if case let .arrayDiff(diff) = value {
                        let attrss: [Results<Attribute>] = allMs.map { $0.attributes.filter("key = %@", attr.attribute) }
                        var attrs: [Attribute] = attrss.flatMap { $0.map { $0 } }
                        if !attrs.isEmpty {
                            let atL = attrs.removeLast()
                            if case let .array(ids) = try? globalScheme.getValue(for: attr.attribute, from: atL.value, in: modif.className) {
                                realm.delete(attrs)
                                let orig = diff.original
                                let localDiff = ids.difference(from: orig)
                                if let current = orig.applying(diff.difference + localDiff) {
                                    obj.setAttribute(attr.attribute, to: .array(current))
                                    atL.value = try! globalScheme.getData(for: attr.attribute, value: .array(current), in: modif.className)
                                }
                            }
                        } else {
                            obj.setAttribute(attr.attribute, to: value)
                        }
                    } else if allMs.isEmpty {
                        obj.setAttribute(attr.attribute, to: value)
                    }
                }
            }
        }
        guard let log = realm.object(ofType: Log.self, forPrimaryKey: diff.syncLogId) else {
            return
        }
        realm.delete(log, cascading: true)
    }

    func integrate(msg: DownstreamMessage) {
        defer { currentMessage = nil }
        onSyncMainThread {
            let realm = getRealm()!
            switch msg {
            case let .diff(diff, pullRequest):
                realm.beginWrite()
                integrateDiff(diff, with: pullRequest, on: realm)
                try? realm.commitWrite(withoutNotifying: allNotificationTokens)
            case .groupRemoved(let logId, let groupId):
                let realm = getRealm()!
                guard let log = realm.object(ofType: Log.self, forPrimaryKey: logId) else {
                    return
                }
                realm.beginWrite()
                let deleteGroupLogs: [Patch] = log.deletions.filter("objectId = %@", groupId).map { $0 }
                deleteGroupLogs.forEach { patch in
                    realm.delete(patch, cascading: true)
                }
                if let group = realm.object(ofType: SyncGroup.self, forPrimaryKey: groupId) {
                    realm.delete(log, cascading: true)
                    for obj in allObservingObjects {
                        let all = realm.objects(obj.self).filter("group.id = '\(groupId)'")
                        realm.delete(all)
                    }
                    realm.delete(group, cascading: true)
                }
                try? realm.commitWrite(withoutNotifying: allNotificationTokens)
            case .groupCreated(let logId, let groupId), .failedGroupCreate(let logId, let groupId):
                guard let log = realm.object(ofType: Log.self, forPrimaryKey: logId) else {
                    return
                }
                try? realm.write {
                    let createGroupLogs: [Patch] = log.creations.filter("objectId = %@", groupId).map { $0 }
                    createGroupLogs.forEach { patch in
                        realm.delete(patch, cascading: true)
                    }
                    checkGroupLog(log: log, groupId: groupId, realm: realm)
                }
            case .subscribed(let logId, let userId, let groupId), .failedSubscribe(let logId, let userId, let groupId):
                guard let log = realm.object(ofType: Log.self, forPrimaryKey: logId) else {
                    return
                }
                guard let group = realm.object(ofType: SyncGroup.self, forPrimaryKey: groupId) else {
                    return
                }
                realm.beginWrite()
                let modifyGroupLogs: [Attribute] = log.modifications.filter("objectId = %@", groupId).map { $0 }.flatMap { $0.attributes.filter("key = 'toSubscribe'") }
                modifyGroupLogs.forEach { attr in
                    guard case let .array(ids) = try? globalScheme.getValue(for: "toSubscribe", from: attr.value, in: "SyncGroup") else {
                        preconditionFailure()
                    }
                    let newIds = ids.filter { $0 != userId }
                    attr.value = try! globalScheme.getData(for: "toSubscribe", value: .array(newIds), in: "SyncGroup")
                }
                group.toSubscribe.forEach { subs in
                    guard subs.name == userId else { return }
                    realm.delete(subs)
                }
                checkGroupLog(log: log, groupId: groupId, realm: realm)
                try? realm.commitWrite(withoutNotifying: allNotificationTokens)
            case .unsubscribed(let logId, let userId, let groupId):
                guard let log = realm.object(ofType: Log.self, forPrimaryKey: logId) else {
                    return
                }
                guard let group = realm.object(ofType: SyncGroup.self, forPrimaryKey: groupId) else {
                    return
                }
                realm.beginWrite()
                let modifyGroupLogs: [Attribute] = log.modifications.filter("objectId = %@", groupId).map { $0 }.flatMap { $0.attributes.filter("key = 'toUnsubscribe'") }
                modifyGroupLogs.forEach { attr in
                    guard case let .array(ids) = try? globalScheme.getValue(for: "toUnsubscribe", from: attr.value, in: "SyncGroup") else {
                        preconditionFailure()
                    }
                    let newIds = ids.filter { $0 != userId }
                    attr.value = try! globalScheme.getData(for: "toUnsubscribe", value: .array(newIds), in: "SyncGroup")
                }
                group.toUnsubscribe.forEach { subs in
                    guard subs.name == userId else { return }
                    realm.delete(subs)
                }
                checkGroupLog(log: log, groupId: groupId, realm: realm)
                try? realm.commitWrite(withoutNotifying: allNotificationTokens)
            }
        }
    }

    private func checkGroupLog(log: Log, groupId: String, realm: Realm) {
        guard let group = realm.object(ofType: SyncGroup.self, forPrimaryKey: groupId) else {
            return
        }
        if group.toSubscribe.isEmpty && group.toUnsubscribe.isEmpty && !group.isDeleted {
            realm.delete(log.creations.filter("objectId = %@", groupId), cascading: true)
            realm.delete(log.deletions.filter("objectId = %@", groupId), cascading: true)
            realm.delete(log.modifications.filter("objectId = %@", groupId), cascading: true)
        }
        if log.creations.isEmpty && log.deletions.isEmpty && log.modifications.isEmpty {
            realm.delete(log, cascading: true)
        }
    }
}
