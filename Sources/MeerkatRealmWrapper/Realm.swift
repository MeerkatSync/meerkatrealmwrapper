//
//  Realm.swift
//  
//
//  Created by Filip Klembara on 15/02/2020.
//

private var _defaultMeerkatRealm = Realm.Configuration.defaultConfiguration

public extension Realm.Configuration {
    static var meerkatRealmConfiguration: Realm.Configuration {
        get {
            _defaultMeerkatRealm
        }
        set {
            _defaultMeerkatRealm = newValue
        }
    }
}

func tryGetRealm() throws -> Realm {
    try Realm(configuration: Realm.Configuration.meerkatRealmConfiguration)
}

func getRealm() -> Realm? {
    try? tryGetRealm()
}
