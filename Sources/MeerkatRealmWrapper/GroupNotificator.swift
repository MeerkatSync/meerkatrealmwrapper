//
//  GroupNotificator.swift
//  
//
//  Created by Filip Klembara on 23/02/2020.
//

import Foundation
import Combine

extension Publisher where Output == MeerkatRTransaction {
    public func groupChangePriority() -> Publishers.GroupChangePrioriter<Self> {
        Publishers.GroupChangePrioriter(upstream: self)
    }
}

extension Publishers {
    public final class GroupChangePrioriter<Upstream: Publisher>: Publisher where Upstream.Output == MeerkatRTransaction {
        public func receive<S>(subscriber: S) where S : Subscriber, GroupChangePrioriter.Failure == S.Failure, GroupChangePrioriter.Output == S.Input {
            upstream.subscribe(Inner(downstream: subscriber))
        }

        public typealias Output = GroupUpdate

        public typealias Failure = Upstream.Failure

        private let upstream: Upstream

        public init(upstream: Upstream) {
            self.upstream = upstream
        }
    }
}

public enum GroupUpdate {
    case add(logId: String, name: String)
    case remove(logId: String, name: String)
    case subscribe(logId: String, userId: String, role: Role, groupId: String)
    case unsubscribe(logId: String, userId: String, groupId: String)
}

extension Publishers.GroupChangePrioriter {
    fileprivate final class Inner<Downstream: Subscriber> where Downstream.Input == GroupUpdate {
        private let downstream: Downstream
        private var status = SubscriptionStatus.awaitingSubscription
        private let mutex = DispatchSemaphore(value: 1)
        private var demands: Subscribers.Demand = .none
        private var current: MeerkatRTransaction? = nil
        private var closedSubs = [String]()
        private var closedUnsubs = [String]()

        init(downstream: Downstream) {
            self.downstream = downstream
        }

        var isSubscribed: Bool {
            switch status {
            case .subscribed:
                return true
            default:
                return false
            }
        }
    }
}

extension Publishers.GroupChangePrioriter.Inner: Subscription {
    func request(_ demand: Subscribers.Demand) {
        mutex.wait()
        guard isSubscribed else {
            mutex.signal()
            return
        }
        demands += demand
        lockedTrySend()
        mutex.signal()
    }

    private func splitModification(logId: String, _ modif: MeerkatRSubtransaction, syncId: String) -> [(GroupUpdate, Date)] {
        guard let realm = getRealm() else {
            return []
        }
        return modif.updates.flatMap { u -> [(GroupUpdate, Date)] in
            guard u.attribute == "toSubscribe" || u.attribute == "toUnsubscribe", case let .array(ids) = u.value else {
                return []
            }
            let alls = ids.map { "'\($0)'" }.joined(separator: ",")
            let arr: [UserSubscription] = realm.objects(UserSubscription.self).filter("id IN {\(alls)}").map { $0 }
            if u.attribute == "toSubscribe" {
                let a = arr.filter { !closedSubs.contains(modif.objectId + "@" + $0.name) }
                return a.map { (.subscribe(logId: logId, userId: $0.name, role: Role(rawValue: $0.role)!, groupId: modif.objectId), modif.timestamp) }
            } else {
                let a = arr.filter { !closedUnsubs.contains(modif.objectId + "@" + $0.name) }
                return a.map { (.unsubscribe(logId: logId, userId: $0.name, groupId: modif.objectId), modif.timestamp) }
            }
        }
    }

    private func lockedTrySend() {
        guard demands > .none else {
            return
        }
        let cs = (current?.creations ?? []).map { (GroupUpdate.add(logId: current!.syncLogId, name: $0.object.objectId), $0.object.timestamp) }
        let ds = (current?.deletions ?? []).map { (GroupUpdate.remove(logId: current!.syncLogId, name: $0.objectId), $0.timestamp) }
        let ms = (current?.modifications ?? []).flatMap { splitModification(logId: current!.syncLogId, $0, syncId: current?.syncLogId ?? "") }
        let t = cs.first ?? (ms + ds).sorted { $0.1 < $1.1 }.first
        guard let curr = current, let (trans, _) = t else {
            current = nil
            closedSubs.removeAll()
            closedUnsubs.removeAll()
            guard case let .subscribed(sub) = status else {
                return
            }
            mutex.signal()
            sub.request(.max(1))
            mutex.wait()
            return
        }

        switch trans {
        case .add(_, let name):
            current = .init(syncLogId: curr.syncLogId, deletions: curr.deletions, creations: curr.creations.filter { $0.object.objectId != name }, modifications: curr.modifications)
        case .remove(_, let name):
            current = .init(syncLogId: curr.syncLogId, deletions: curr.deletions.filter { $0.objectId != name }, creations: curr.creations, modifications: curr.modifications)
        case .subscribe(_, let userId, _, let groupId):
            closedSubs.append(groupId + "@" + userId)
        case .unsubscribe(_, let userId, let groupId):
            closedUnsubs.append(groupId + "@" + userId)
        }
        mutex.signal()
        let d = downstream.receive(trans)
        mutex.wait()
        demands -= 1
        let dd = demands + d
        demands = .none
        mutex.signal()
        request(dd)
        mutex.wait()
    }

    func cancel() {
        mutex.wait()
        defer { mutex.signal() }
        guard case let .subscribed(sub) = status else {
            return
        }
        sub.cancel()
        status = .terminated
        closedSubs.removeAll()
        closedUnsubs.removeAll()
        current = nil
    }
}

extension Publishers.GroupChangePrioriter.Inner: Subscriber {
    func receive(subscription: Subscription) {
        mutex.wait()

        guard case .awaitingSubscription = status else {
            subscription.cancel()
            mutex.signal()
            return
        }
        status = .subscribed(subscription: subscription)
        mutex.signal()
        downstream.receive(subscription: self)
    }

    func receive(_ input: MeerkatRTransaction) -> Subscribers.Demand {
        mutex.wait()
        defer { mutex.signal() }
        guard isSubscribed else {
            return .none
        }
        assert(current == nil)
        assert(closedUnsubs.isEmpty)
        assert(closedSubs.isEmpty)
        current = input
        lockedTrySend()
        if demands > .none {
            return .max(1)
        } else {
            return .none
        }
    }

    func receive(completion: Subscribers.Completion<Downstream.Failure>) {
        mutex.wait()
        guard isSubscribed else {
            mutex.signal()
            return
        }
        mutex.signal()
        downstream.receive(completion: completion)
    }

    typealias Input = MeerkatRTransaction
}

