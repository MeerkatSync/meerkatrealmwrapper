//
//  RealmSyncObject+observableObject.swift
//  
//
//  Created by Filip Klembara on 19/04/2020.
//

extension RealmSyncObject where Self: Object {
    public func observableObject() -> ObservableSyncObject<Self> {
        .init(object: self)
    }

    public func observableObject<A>(ignore a: KeyPath<Self, A>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a)
    }

    public func observableObject<A, B>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b)
    }

    public func observableObject<A, B, C>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c)
    }

    public func observableObject<A, B, C, D>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d)
    }

    public func observableObject<A, B, C, D, E>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e)
    }

    public func observableObject<A, B, C, D, E, F>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>, _ f: KeyPath<Self, F>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e, f)
    }

    public func observableObject<A, B, C, D, E, F, G>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>, _ f: KeyPath<Self, F>, _ g: KeyPath<Self, G>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e, f, g)
    }

    public func observableObject<A, B, C, D, E, F, G, H>(ignore a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>, _ f: KeyPath<Self, F>, _ g: KeyPath<Self, G>, _ h: KeyPath<Self, H>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e, f, g, h)
    }
    public func observableObject<A>(observe a: KeyPath<Self, A>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a)
    }

    public func observableObject<A, B>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b)
    }

    public func observableObject<A, B, C>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c)
    }

    public func observableObject<A, B, C, D>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d)
    }

    public func observableObject<A, B, C, D, E>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e)
    }

    public func observableObject<A, B, C, D, E, F>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>, _ f: KeyPath<Self, F>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e, f)
    }

    public func observableObject<A, B, C, D, E, F, G>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>, _ f: KeyPath<Self, F>, _ g: KeyPath<Self, G>) -> ObservableSyncObject<Self> {
        .init(object: self, ignore: a, b, c, d, e, f, g)
    }

    public func observableObject<A, B, C, D, E, F, G, H>(observe a: KeyPath<Self, A>, _ b: KeyPath<Self, B>, _ c: KeyPath<Self, C>, _ d: KeyPath<Self, D>, _ e: KeyPath<Self, E>, _ f: KeyPath<Self, F>, _ g: KeyPath<Self, G>, _ h: KeyPath<Self, H>) -> ObservableSyncObject<Self> {
        .init(object: self, observe: a, b, c, d, e, f, g, h)
    }
}
