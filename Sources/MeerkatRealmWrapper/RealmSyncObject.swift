//
//  RealmSyncObject.swift
//
//
//  Created by Filip Klembara on 17/02/2020.
//

import Foundation

public protocol RealmSyncObject: SynchronizableObject, SchemeDescribable {
    var isDeleted: Bool { get }
    var group: SyncGroup? { get }
}

extension RealmSyncObject {
    public var isInSharedGroup: Bool {
        guard let g = group else { return false }
        guard g.id != SyncGroupDefaults.defaultName else { return false }
        return true
    }
}

extension List: SchemeAttribute where Element: SyncObject {
    public var attributeType: ClassAttributesDescription.AttributeType {
        [Element]().attributeType
    }
}

extension LinkingObjects: SchemeAttribute {
    public var ignore: Bool { true }
    public var attributeType: ClassAttributesDescription.AttributeType { fatalError("LinkingObjects does not have synchronizable attribute type") }
}

extension Object {
    func attributeData(for attribute: String, value: UpdateValue) throws -> Data? {
        try globalScheme.getData(for: attribute, value: value, in: self.objectSchema.className)
    }

    func attributeValue(for attribute: String, from data: Data?) throws -> UpdateValue {
        try globalScheme.getValue(for: attribute, from: data, in: self.objectSchema.className)
    }
}

extension RealmSyncObject where Self: Object {
    static func registerNotification(realm: Realm,
                                     onUpdate update: @escaping (_ log: Log) -> Void) -> NotificationToken {
        return realm.objects(Self.self).observe { changes in
            switch changes {
            case .initial:
                return
            case let .update(objects, deletions, insertions, modifications):
                assert(deletions.isEmpty, "You should not use `realm.delete(_:)`, use isDeleted attribute instead")
                let ins = insertions.map { objects[$0] }
                let modifs = modifications.map { objects[$0] }
                let log = asLog(realm: objects.realm!, insertions: ins, modifications: modifs)
                DispatchQueue.global(qos: .utility).async {
                    update(log)
                }
            case .error(let err):
                print("error", err)
            }
        }
    }

    private static func asLog(realm: Realm, insertions: [Self], modifications: [Self]) -> Log {
        var ins = [Patch]()
        var toDelete = [Self]()
        var validIns = [Self]()
        insertions.forEach { object in
            if object.isDeleted {
                toDelete.append(object)
            } else {
                validIns.append(object)
                ins.append(object.toPatch(updateType: .create))
            }
        }
        realm.delete(toDelete)
        var del = [Patch]()
        var mod = [Patch]()
        let modifs = modifications + validIns
        modifs.forEach { object in
            if object.isDeleted {
                del.append(object.toPatch(updateType: .delete))
            } else {
                mod.append(object.toPatch(updateType: .update))
            }
        }
        let log = Log()
        log.deletions.append(objectsIn: del)
        log.creations.append(objectsIn: ins)
        log.modifications.append(objectsIn: mod)
        return log
    }

    func toPatch(updateType: LocalUpdateType) -> Patch {
        let schema = objectSchema
        var attributes = [Attribute]()
        for prop in schema.properties {
            let attribute = Attribute()
            let label = prop.name
            if label == "isDeleted" || label == "id" {
                continue
            }
            attribute.key = label
            if updateType == .update || label == "group" {
                let updateValue: UpdateValue
                if let value = self[label] {
                    guard let _updateValue = valueToUpdateValue(type: prop, name: label, value: value) else {
                        fatalError("Unsupported type for key '\(label)', value '\(value)' in object '\(self)'")
                    }
                    updateValue = _updateValue
                } else {
                    updateValue = .nil
                }
                let data = try! attributeData(for: label, value: updateValue)
                attribute.value = data
                attributes.append(attribute)
            }
        }
        let patch = Patch()
        patch.objectId = id
        patch.updateType = updateType.rawValue
        patch.objectClassName = Self.className()
        patch.attributes.append(objectsIn: attributes)
        return patch
    }

    func setAttribute(_ name: String, to value: UpdateValue) {
        let props = objectSchema.properties
        guard let prop = props.first(where: { $0.name == name }) else {
            return
        }
        switch (value, prop.type) {
        case (.string(let s), .string):
            self[name] = s
        case let (.int(i), .int):
            self[name] = i
        case let (.uint(i), .int):
            self[name] = Int(i)
        case let (.data(d), .data):
            self[name] = d
        case let (.bool(b), .bool):
            self[name] = b
        case let (.double(d), .double):
            self[name] = d
        case let (.double(d), .float):
            self[name] = d
        case (.nil, _):
            guard prop.isOptional else { return }
            self[name] = nil
        case let (.date(d), .date):
            self[name] = d
        case let (.object(id), .object):
            guard !prop.isArray, let refClass = prop.objectClassName else {
                return
            }
            guard let obj = realm?.dynamicObject(ofType: refClass, forPrimaryKey: id) else {
                self[name] = nil
                return
            }
            self[name] = obj
        case let (.array(ids), .object):
            guard prop.isArray, let refClass = prop.objectClassName else {
                return
            }
            let list = dynamicList(name)
            list.removeAll()
            let objs = ids.compactMap { realm?.dynamicObjects(refClass).filter("id = %@", $0).first }
            list.append(objectsIn: objs)
        case let (.arrayDiff(diff), .object):
            guard prop.isArray else {
                return
            }
            let oldIds = diff.original
            let list = dynamicList(name)
            let currentIds: [String] = list.map { ($0 as! SyncObject).id }
            let localDiff = oldIds.difference(from: currentIds)
            guard let new = oldIds.applying(localDiff + diff.difference) else {
                return
            }
            setAttribute(name, to: .array(new))
        default:
            return
        }
    }

    private func valueToUpdateValue(type: Property, name: String, value: Any) -> UpdateValue? {
        switch type.type {
        case .int:
            guard let i = value as? Int else {
                return nil
            }
            return .int(i)
        case .bool:
            guard let b = value as? Bool else {
                return nil
            }
            return .bool(b)
        case .float:
            guard let v = value as? Float else {
                return nil
            }
            return .double(Double(v))
        case .double:
            guard let v = value as? Double else {
                return nil
            }
            return .double(v)
        case .string:
            guard let v = value as? String else {
                return nil
            }
            return .string(v)
        case .data:
            guard let v = value as? Data else {
                return nil
            }
            return .data(v)
        case .any:
            fatalError("Not Implemented")
        case .date:
            guard let v = value as? Date else {
                return nil
            }
            return .date(v)
        case .object:
            if type.isArray {
                let list = dynamicList(name)
                let ids: [String] = list.map { $0 as! SyncObject }.map { $0.id }
                return .array(ids)
            } else {
                guard let object = self[name] as? SyncObject else {
                    return nil
                }
                return .object(id: object.id)
            }
        case .linkingObjects:
            fatalError("Not Implemented")
        }
    }

    public static var all: Results<Self> {
        let realm = getRealm()!
        return realm.objects(Self.self).filter("isDeleted = false")
    }

    public var wrapped: SyncObjectWrapper<Self> {
        .init(object: self)
    }

    public static var observableObjects: ObservableSyncObjects<Self> {
        .init()
    }
}
