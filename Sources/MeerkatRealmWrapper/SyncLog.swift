//
//  SyncLog.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

struct SyncLog {
    let id: String
}

extension SyncLog {
    var log: Log? {
        guard let realm = getRealm() else {
            return nil
        }
        return getLog(on: realm)
    }

    func getLog(on realm: Realm) -> Log? {
        realm.object(ofType: Log.self, forPrimaryKey: id)
    }
}
