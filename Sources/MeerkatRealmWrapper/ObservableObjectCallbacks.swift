//
//  ObservableObjectCallbacks.swift
//  
//
//  Created by Filip Klembara on 24/04/2020.
//

import Foundation

class ObservableObjectCallbacks {

    static let shared = ObservableObjectCallbacks()

    private init() { }

    typealias Callback = () -> Void

    private var dict = [String: [ObjectIdentifier: [Callback]]]()

    private let mutex = DispatchSemaphore(value: 1)

    func getKey(className: String, objectId id: String) -> String {
        "\(className)@\(id)"
    }

    func getKey<T: SyncObject>(for object: SyncObjectWrapper<T>) -> String {
        getKey(className: object.objectSchema.className, objectId: object.id)
    }

    func subscribe<T: SyncObject>(object: SyncObjectWrapper<T>, with id: ObjectIdentifier, callback: @escaping Callback) {
        let key = getKey(for: object)
        mutex.wait()
        defer { mutex.signal() }
        var inner = dict[key] ?? [:]
        let arr = inner[id] ?? []
        inner[id] = arr + [callback]
        dict[key] = inner
    }

    private func unsubscribe(key: String, id: ObjectIdentifier) {
        mutex.wait()
        defer { mutex.signal() }
        var inner = dict[key] ?? [:]
        inner[id] = nil
        dict[key] = inner
    }

    func unsubscribe<T: SyncObject>(object: SyncObjectWrapper<T>, with id: ObjectIdentifier) {
        let key = getKey(for: object)
        DispatchQueue.main.async {
            self.unsubscribe(key: key, id: id)
        }
    }

    func finish<T: SyncObject>(object: SyncObjectWrapper<T>) {
        let key = getKey(for: object)
        mutex.wait()
        let inner = dict[key] ?? [:]
        dict[key] = nil
        mutex.signal()
        inner.flatMap { $0.value }.forEach { $0() }
    }

    func finish<T: SyncObject>(object: SyncObjectWrapper<T>, with id: ObjectIdentifier) {
        let key = getKey(for: object)
        mutex.wait()
        var inner = dict[key] ?? [:]
        let cbs = inner[id]
        inner[id] = nil
        dict[key] = inner
        mutex.signal()
        cbs?.forEach { $0() }
    }
}
