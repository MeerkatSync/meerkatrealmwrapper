//
//  MeerkatRealmNotificator.swift
//
//
//  Created by Filip Klembara on 15/02/2020.
//

import Combine
import Foundation

public typealias SyncObject = Object & RealmSyncObject

private let realmNotificationsMutex = DispatchSemaphore(value: 1)
private var notificationTokens = [String: NotificationToken]()

func allNotificationTokensBut(_ type: SyncObject.Type) -> [NotificationToken] {
    realmNotificationsMutex.wait()
    defer { realmNotificationsMutex.signal() }
    let exp = type.className()
    return notificationTokens.filter { $0.key != exp }.map { $0.value }
}

var allNotificationTokens: [NotificationToken] {
    realmNotificationsMutex.wait()
    defer { realmNotificationsMutex.signal() }
    return Array(notificationTokens.values)
}

private let realmObservingMutex = DispatchSemaphore(value: 1)
private var observingObjects = [String: [SyncObject.Type]]()

var allObservingObjects: [SyncObject.Type] {
    realmObservingMutex.wait()
    defer { realmObservingMutex.signal() }
    return observingObjects.values.flatMap { $0 }
}

@available(OSX 10.15, *)
final class MeerkatRealmNotificator {
    enum MRWError: Error {
        case realmError(Error)
    }
    private let tokensId = UUID().uuidString
    private let subject: PassthroughSubject<Log, MRWError> = .init()
    private let obsObjects: [SyncObject.Type]
    init(objects: [SyncObject.Type]) {
        self.obsObjects = objects
        precondition(RunLoop.current == RunLoop.main)
        let realm: Realm
        do {
            realm = try tryGetRealm()
        } catch {
            self.subject.send(completion: .failure(.realmError(error)))
            return
        }
        objects.forEach {
            addNewClassToScheme(class: $0.schemeDescription)
        }

        let nt = objects.map { t -> (String, NotificationToken) in
            let tok = t.registerNotification(realm: realm) { [weak self] log in
                guard let s = self else {
                    return
                }
                s.subject.send(log)
            }
            let type = t.className()
            return (type, tok)
        }
        realmNotificationsMutex.wait()
        defer { realmNotificationsMutex.signal() }
        nt.forEach { notificationTokens[$0] = $1 }

        realmObservingMutex.wait()
        defer { realmObservingMutex.signal() }
        observingObjects[tokensId] = obsObjects

    }

    deinit {
        realmNotificationsMutex.wait()
        defer { realmNotificationsMutex.signal() }
        self.obsObjects.forEach { t in
            notificationTokens[t.className()]?.invalidate()
            notificationTokens[t.className()] = nil
        }
//        if let nt = notificationTokens[tokensId] {
//            notificationTokens[tokensId] = nil
//            nt.forEach { $0.invalidate() }
//        }
        realmObservingMutex.wait()
        defer { realmObservingMutex.signal() }
        observingObjects[tokensId] = nil
    }
}

@available(OSX 10.15, *)
extension MeerkatRealmNotificator: Publisher {
    func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
        subject.receive(subscriber: subscriber)
    }

    typealias Output = Log

    typealias Failure = MRWError
}
