//
//  Publisher+asCodableTransaction.swift
//  
//
//  Created by Filip Klembara on 20/02/2020.
//

import Combine

extension Publisher where Output == MeerkatRPush {
    public func asCodablePushTransaction() -> Publishers.MapKeyPath<Self, LogCodablePush> {
        map(\.asCodable)
    }
}

extension MeerkatRPush {
    fileprivate var asCodable: LogCodablePush {
        .init(syncLogId: syncLogId, push: .init(schemeVersion: 0, groups: groups.map { $0.asCodable }, transaction: transaction.asCodable))
    }
}

extension MeerkatRGroup {
    fileprivate var asCodable: CodableGroup {
        .init(id: id, version: UInt64(version))
    }
}

extension MeerkatRTransaction {
    fileprivate var asCodable: CodableTransaction {
        .init(deletions: deletions.map { $0.asCodable },
              creations: creations.map { $0.asCodable },
              modifications: modifications.map { $0.asCodable })
    }
}

extension MeerkatRSubtransaction {
    fileprivate var asCodable: CodableSubtransaction {
        .init(timestamp: timestamp,
              updates: updates.map { $0.asCodable(className: className) },
              className: className,
              objectId: objectId)
    }
}

extension MeerkatRUpdate {
    fileprivate func asCodable(className: String) -> CodableUpdate {
        try! .init(attribute: attribute, value: globalScheme.getData(for: attribute, value: value, in: className))
    }
}

extension MeerkatRCreationDescription {
    fileprivate var asCodable: CodableCreationDescription {
        .init(groupID: groupID, object: object.asCodable)
    }
}
