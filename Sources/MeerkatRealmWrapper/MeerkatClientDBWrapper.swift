//
//  MeerkatClientDBWrapper.swift
//  
//
//  Created by Filip Klembara on 28/02/2020.
//

import Combine

public struct MeerkatClientDBWrapper: ClientDBWrapper {
    public func dropSyncObjects() throws {
        let realm = try tryGetRealm()
        realm.beginWrite()
        objects.forEach {
            let objs = realm.objects($0.self)
            realm.delete(objs)
        }
        let groups = realm.objects(SyncGroup.self)
        realm.delete(groups, cascading: true)
        let attributes = realm.objects(Attribute.self)
        realm.delete(attributes, cascading: true)
        let patches = realm.objects(Patch.self)
        realm.delete(patches, cascading: true)
        let logs = realm.objects(Log.self)
        realm.delete(logs, cascading: true)
        try realm.commitWrite(withoutNotifying: allNotificationTokens)
    }

    private let onError: (Failure) -> Void
    private let objects: [SyncObject.Type]
    private let objectNotificator: AnyPublisher<SyncLog, MeerkatRealmNotificator.MRWError>
    private let groupNotificator: AnyPublisher<SyncLog, MeerkatRealmNotificator.MRWError>
    public init(objects: [SyncObject.Type], onError: @escaping (Failure) -> Void) {
        self.onError = onError
        self.objects = objects
        onSyncMainThread {
            let realm = getRealm()!
            if realm.object(ofType: SyncGroup.self, forPrimaryKey: SyncGroupDefaults.defaultName) == nil {
                let realm = getRealm()!
                realm.beginWrite()
                let g = SyncGroup()
                g.id = SyncGroupDefaults.defaultName
                realm.add(g)
                try! realm.commitWrite(withoutNotifying: allNotificationTokens)
            }
        }
        objectNotificator = MeerkatRealmNotificator(objects: objects).storeLogs(isGroup: false).eraseToAnyPublisher()
        groupNotificator = MeerkatRealmNotificator(objects: [SyncGroup.self]).storeLogs(isGroup: true).eraseToAnyPublisher()
    }

    public func dbPublisher() -> Upstream {
        MeerkatDBPublisher(objectPublisher: objectNotificator, groupPublisher: groupNotificator)
    }

    public func dbSubscriber() -> Downstream {
        .init(onError: onError)
    }

    public typealias Failure = Error

    public typealias Downstream = Subscribers.ChangeIntegrator<Failure>

    public typealias Upstream = AnyPublisher<UpstreamMessage, Failure>
}
