//
//  ObjectCache.swift
//  
//
//  Created by Filip Klembara on 24/04/2020.
//

import Foundation

open class ObjectCache<T: Object> {

    public init() { }

    public init(object: T) {
        recache(with: object)
    }

    private var cache: [String: Any] = [:]

    private lazy var properties: [String] = T().objectSchema.properties.map { $0.name }

    open func value<U>(for attribute: String, resultType: U.Type = U.self) -> U? {
        cache[attribute] as? U
    }

    open func value<U>(for keyPath: KeyPath<T, U>) -> U? {
        let label = NSExpression(forKeyPath: keyPath).keyPath
        return value(for: label)
    }

    open func set<U>(value: U, for keyPath: WritableKeyPath<T, U>) {
        let label = NSExpression(forKeyPath: keyPath).keyPath
        cache[label] = value
    }

    open func recache(with object: T) {
        guard !object.isInvalidated else { return }
        properties.forEach {
            cache[$0] = object[$0]
        }
    }
}
