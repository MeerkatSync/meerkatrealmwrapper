//
//  Patch.swift
//  
//
//  Created by Filip Klembara on 18/02/2020.
//

import Foundation

final class Patch: Object {
    @objc dynamic var objectId = ""
    @objc dynamic var updateType = ""
    @objc dynamic var created = Date()
    @objc dynamic var objectClassName = ""
    @objc dynamic var inSync = false
    let attributes = List<Attribute>()

    override static func indexedProperties() -> [String] {
        return ["objectId", "objectClassName", "updateType"]
    }

    func correctGroup(groups: Set<String>) {
        guard let attr = attributes.filter({ $0.key == "group" }).first else {
            return
        }
        var name: String!
        switch try! globalScheme.getValue(for: "group", from: attr.value, in: objectClassName) {
        case let .object(id):
            guard let group = groups.first(where: { $0 == id }) else {
                assertionFailure("Unknown group")
                fallthrough
            }
            name = group
        case .nil:
            name = SyncGroupDefaults.defaultName
            let clsName = objectClassName
            let objId = objectId
            onSyncMainThread {
                let realm = getRealm()!
                realm.beginWrite()

                let group: SyncGroup
                if let g = realm.object(ofType: SyncGroup.self, forPrimaryKey: name) {
                    group = g
                } else {
                    let g = SyncGroup()
                    g.id = name
                    realm.add(g)
                    group = g
                }
                let obj = realm.dynamicObjects(clsName).filter("id = %@", objId).first! as! SyncObject
                obj["group"] = group
                try! realm.commitWrite(withoutNotifying: allNotificationTokens)
            }
        default:
            fatalError("Invalid group type")
        }
        try! realm!.write {
            attr.value = try! globalScheme.getData(for: "group", value: .object(id: name), in: objectClassName)
        }
    }
}

extension Results where Element == Patch {
    func pending(_ type: LocalUpdateType) -> Results<Patch> {
        return filter("updateType == %@", type.rawValue)
    }

    func by(className: String, id: String) -> Results<Patch> {
        return filter("objectClassName == %@ && objectId == %@", className, id)
    }

//    func toObjectDiff(as updateType: LocalUpdateType) -> [ObjectDiff] {
//        return map { patch in
//            let patchAttributes: [PatchAttribute]
//            switch updateType {
//            case .delete:
//                patchAttributes = []
//            default:
//                patchAttributes = patch.attributes.map { $0.toPatchAttribute() }
//            }
//            return ObjectDiff(className: patch.objectClassName,
//                              objectId: patch.objectId,
//                              changes: patchAttributes,
//                              created: patch.created)
//        }
//    }
}

final class Attribute: Object {
    @objc dynamic var key = ""
    @objc dynamic var value: Data?
    let patch = LinkingObjects(fromType: Patch.self, property: "attributes")

//    fileprivate func toPatchAttribute() -> PatchAttribute {
//        if let data = value {
//            return PatchAttribute(key: key, value: data)
//        } else {
//            return PatchAttribute(key: key)
//        }
//    }
}

enum LocalUpdateType: String {
    case create
    case update
    case delete
}
